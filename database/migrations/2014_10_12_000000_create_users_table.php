<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_user', function (Blueprint $table) {
            $table->id('id_user');
            $table->string('nama');
            $table->string('username')->unique();
            $table->string('password');
            $table->string('role');
            $table->rememberToken();
            $table->timestamps();
        });

        DB::table('tb_user')->insert([
            'id_user' => '1',
            'nama' => 'admin',
            'username' => 'admin',
            'password' => '$2y$10$hzU5/mgnQ9FmT56nuU6MButauFrYyxmtbc4hoFtn7ncvMYv/NKvBG',
            'role' => 'ADMIN',
            'created_at' => Carbon::now(),
        ]);

        DB::table('tb_user')->insert([
            'id_user' => '2',
            'nama' => 'kasubag',
            'username' => 'kasubag',
            'password' => '$2y$10$Vv36zJZQdOUr7//K/PbQwezrhIoZWFJFZaFzPkBwnio2HOyHX/rTu',
            'role' => 'KASUBAG',
            'created_at' => Carbon::now(),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_user');
    }
}
