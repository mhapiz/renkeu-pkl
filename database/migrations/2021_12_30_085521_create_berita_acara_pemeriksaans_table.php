<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBeritaAcaraPemeriksaansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_berita_acara_pemeriksaan', function (Blueprint $table) {
            $table->id('id_berita_acara_pemeriksaan');
            $table->string('no_surat');
            $table->unsignedInteger('surat_pengadaan_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_berita_acara_pemeriksaan');
    }
}
