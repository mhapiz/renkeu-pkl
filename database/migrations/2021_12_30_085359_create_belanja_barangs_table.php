<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBelanjaBarangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_belanja_barang', function (Blueprint $table) {
            $table->id('id_belanja_barang');
            $table->date('tanggal');
            $table->enum('kategori', ['Materai', 'ATK', 'Alat Kebersihan', 'Alat Listrik']);
            $table->unsignedInteger('suplier_id');
            $table->unsignedInteger('pegawai_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_belanja_barang');
    }
}
