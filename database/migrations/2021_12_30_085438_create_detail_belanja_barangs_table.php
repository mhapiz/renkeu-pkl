<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailBelanjaBarangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_detail_belanja_barang', function (Blueprint $table) {
            $table->id('id_detail_belanja_barang');
            $table->unsignedInteger('belanja_barang_id');
            $table->string('nama_barang');
            $table->integer('qty');
            $table->string('satuan');
            $table->double('harga_satuan');
            $table->double('jumlah');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_detail_belanja_barang');
    }
}
