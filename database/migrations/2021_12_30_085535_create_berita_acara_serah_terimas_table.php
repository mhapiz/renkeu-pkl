<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBeritaAcaraSerahTerimasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_berita_acara_serah_terima', function (Blueprint $table) {
            $table->id('id_berita_acara_serah_terima');
            $table->string('no_surat');
            $table->unsignedInteger('berita_acara_pemeriksaan_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_berita_acara_serah_terima');
    }
}
