<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class PegawaiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        for ($i = 1; $i <= 100; $i++) {

            // insert data ke table pegawai menggunakan Faker
            DB::table('tb_pegawai')->insert([
                'nama_pegawai' => $faker->name,
                'nip' => $faker->numberBetween(100, 999) . $faker->numberBetween(100, 999) . $faker->numberBetween(100, 999) . $faker->numberBetween(100, 999),
                'jabatan' => $faker->randomElement(['Manager', 'Kasir', 'Koki', 'Pelayan']),
                'nohp' => $faker->e164PhoneNumber,
                'alamat' => $faker->address,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }
    }
}
