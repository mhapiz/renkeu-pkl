<ul class="navbar-nav bg-gradient-success sidebar sidebar-dark accordion" id="accordionSidebar">

    @php
    if(Auth::user()->role == 'ADMIN'){
    $dashboard = 'admin.dashboard';
    }elseif(Auth::user()->role == 'KASUBAG'){
    $dashboard = 'kasubag.dashboard';
    }
    @endphp

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand " href="{{ route($dashboard) }}" style="min-height: 190px">
        <div class="sidebar-brand-icon">
            <img src="{{ asset('assets/banjar.png') }}" alt="" width="60px">
        </div>
        <div class="sidebar-brand-text mx-3 m-0">
            <p class="p-0 m-0">Kecamatan <br> Martapura</p>
        </div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item {{ (request()->is('admin')) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route($dashboard) }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <hr class="sidebar-divider">

    @if (Auth::user()->role == 'ADMIN')
    <!-- Heading -->
    <div class="sidebar-heading">
        Master Data
    </div>


    <li class="nav-item {{ (request()->is('admin/pegawai*')) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('admin.pegawai.index') }}">
            <i class="fas fa-asterisk    "></i>
            <span>Pegawai</span></a>
    </li>

    <li class="nav-item {{ (request()->is('admin/suplier*')) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('admin.suplier.index') }}">
            <i class="fas fa-asterisk    "></i>
            <span>Suplier</span></a>
    </li>

    <li class="nav-item {{ (request()->is('admin/belanja-barang*')) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('admin.belanja-barang.index') }}">
            <i class="fas fa-asterisk    "></i>
            <span>Belanja Barang</span></a>
    </li>


    <hr class="sidebar-divider">

    <div class="sidebar-heading">
        Laporan
    </div>

    <li class="nav-item {{ (request()->is('admin/surat-pengadaan*')) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('admin.surat-pengadaan.index') }}">
            <i class="fas fa-envelope"></i>
            <span>Surat Pengadaan</span></a>
    </li>

    <li class="nav-item {{ (request()->is('admin/berita-acara-pemeriksaan*')) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('admin.berita-acara-pemeriksaan.index') }}">
            <i class="fas fa-envelope"></i>
            <span>Berita Acara Pemeriksaan</span></a>
    </li>

    <li class="nav-item {{ (request()->is('admin/berita-acara-serah-terima*')) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('admin.berita-acara-serah-terima.index') }}">
            <i class="fas fa-envelope"></i>
            <span>Berita Acara Serah Terima</span></a>
    </li>

    @elseif(Auth::user()->role == 'KASUBAG')

    <div class="sidebar-heading">
        Laporan
    </div>

    <li class="nav-item {{ (request()->is('kasubag/surat-pengadaan*')) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('kasubag.surat-pengadaan.index') }}">
            <i class="fas fa-envelope"></i>
            <span>Surat Pengadaan</span></a>
    </li>

    <li class="nav-item {{ (request()->is('kasubag/berita-acara-pemeriksaan*')) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('kasubag.berita-acara-pemeriksaan.index') }}">
            <i class="fas fa-envelope"></i>
            <span>Berita Acara Pemeriksaan</span></a>
    </li>

    <li class="nav-item {{ (request()->is('kasubag/berita-acara-serah-terima*')) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('kasubag.berita-acara-serah-terima.index') }}">
            <i class="fas fa-envelope"></i>
            <span>Berita Acara Serah Terima</span></a>
    </li>

    @endif

    {{--
    <hr class="sidebar-divider d-none d-md-block"> --}}

    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
