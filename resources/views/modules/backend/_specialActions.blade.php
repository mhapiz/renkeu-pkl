<div class="btn-group" role="group" aria-label="Basic example">

    <a class="btn btn-warning btn-edit text-white" href="{!! $editUrl !!}">
        <i class="fas fa-edit"></i>
    </a>

    <a class="btn btn-info btn-detail text-white" href="{!! $detailUrl !!}">
        <i class="fas fa-eye    "></i>
    </a>

    <form action=" {!! $deleteUrl !!} " method="POST">
        @csrf
        @method('delete')
        <button class="btn btn-danger btn-delete rounded-right"
            style="border-bottom-left-radius: 0px; border-top-left-radius: 0px; "
            onclick="return confirm('Apakah Anda Yakin Ingin Menghapus Data Ini?')">
            <i class="fa fa-trash"></i>
        </button>
    </form>

</div>
