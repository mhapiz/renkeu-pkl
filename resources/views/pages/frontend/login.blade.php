@extends('layouts.auth')

@section('title')
Login
@endsection


@section('content')
<div class="row">
    <div class="col-12">
        <div class="px-5 py-4">
            <div class="text-center d-flex mb-4 align-content-center">
                <img src="{{ asset('assets/banjar.png') }}" alt="logo" height="120px" class="mt-2">
                <h1 class="display-4 text-gray-900 m-0 font-weight-bold text-left ml-4">Kecamatan Martapura</h1>
            </div>
            <form class="user" action="{{ route('login.process') }}" method="POST">
                @csrf
                <div class="form-group">
                    <input type="text" name="username" class="form-control form-control-user"
                        placeholder="Masukkan Username ...">
                </div>
                <div class="form-group">
                    <input type="password" name="password" class="form-control form-control-user"
                        placeholder="Masukkan Kata Sandi ...">
                </div>
                <button type="submit" class="btn btn-success btn-user btn-block"> Login</button>
            </form>
        </div>
    </div>
</div>
@endsection
