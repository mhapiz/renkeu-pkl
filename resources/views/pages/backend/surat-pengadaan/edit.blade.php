<div class="modal-body">
    <form action="{{ route('admin.surat-pengadaan.update', $suratPengadaan->id_surat_pengadaan) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label for="no_surat">Nomor Surat</label>
                    <input type="text" class="form-control" id="no_surat" name="no_surat"
                        value="{{ $suratPengadaan->no_surat }}" placeholder="Nomor Surat">
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label for="tanggal">Tanggal</label>
                    <input type="text" class="form-control" id="tanggale" name="tanggal" placeholder="Tanggal"
                        value="{{ $suratPengadaan->tanggal }}">

                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <label>Data Belanja Barang</label>
                    <select class="form-control" id="belanja_barang_id" name="belanja_barang_id">
                        <option value="{{ $suratPengadaan->belanja_barang_id }}" selected>
                            @php
                            $bbs = \App\Models\BelanjaBarang::find($suratPengadaan->belanja_barang_id);
                            @endphp
                            <span class="font-weight-bold">{{ $bbs->kategori }}</span> --
                            {{ Carbon\Carbon::parse($bbs->tanggal)->isoFormat('dddd, D MMMM Y') }} --
                            {{ $bbs->suplier->nama_suplier }}
                        </option>
                        @foreach ($belanjaBarangE as $bbe)
                        <option value="{{ $bbe->id_belanja_barang }}">
                            <span class="font-weight-bold">{{ $bbe->kategori }}</span> --
                            {{ Carbon\Carbon::parse($bbe->tanggal)->isoFormat('dddd, D MMMM Y') }} --
                            {{ $bbe->suplier->nama_suplier }}
                        </option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
    <button type="submit" class="btn btn-info">Update</button>
</div>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://npmcdn.com/flatpickr/dist/l10n/id.js"></script>

<script>
    flatpickr.localize(flatpickr.l10ns.id);
    $( '#tanggale').flatpickr({
        allowInput:true,
        altInput: true,
        altFormat: "j F Y",
        dateFormat: "Y-m-d",
    });

</script>
