@extends('layouts.backend')

@section('title')
Detail Belanja Barang | Admin
@endsection

@section('content')
<!-- Page Heading -->

<div class="card card-body mb-3">
    <div class="row">
        <div class="col-6">
            <h1 class="h3 text-gray-800">Detail Belanja Barang </h1>
        </div>

        <div class="col-6">

        </div>
    </div>
</div>

<div class="card">
    <div class="card-body">

        <div class="row">
            <div class="col-12">
                <a href="{{ route('admin.belanja-barang.index') }}" class="btn btn-secondary mb-3">
                    <i class="fas fa-arrow-left"></i>
                    Kembali</a>
            </div>
            <div class="col-12">
                <table class="table table-bordered" width="100%">
                    <tr>
                        <th>Tanggal</th>
                        <td>{{ \Carbon\Carbon::parse($belanjaBarang->tanggal)->isoFormat('dddd, D MMMM Y') }}</td>
                    </tr>
                    <tr>
                        <th>Kategori</th>
                        <td>Order {{ $belanjaBarang->kategori }}</td>
                    </tr>
                    <tr>
                        <th>Detail Barang</th>
                        <td>
                            <table class="table table-bordered">
                                <thead>
                                    <th>Nama Barang</th>
                                    <th>Banyaknya</th>
                                    <th>Satuan</th>
                                    <th>Harga Satuan</th>
                                    <th>Sub Total</th>
                                </thead>
                                <tbody>
                                    @foreach ($detailBelanjaBarang as $d)
                                    <tr>
                                        <td>{{ $d->nama_barang }}</td>
                                        <td>{{ $d->qty }}</td>
                                        <td>{{ $d->satuan }}</td>
                                        <td>Rp. {{ number_format($d->harga_satuan, 0, ',', '.') }}</td>
                                        <td>Rp. {{ number_format($d->jumlah, 0, ',', '.') }}</td>
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="4" class="text-center font-weight-bold"> Total </td>
                                        <td class="font-weight-bold">
                                            @php
                                            $total = 0;
                                            foreach ($detailBelanjaBarang as $de) {
                                            $total += $de->jumlah ;
                                            }
                                            @endphp
                                            {{ 'Rp. ' . number_format($total,0,',','.') }}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
