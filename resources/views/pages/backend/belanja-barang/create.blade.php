@extends('layouts.backend')

@section('title')
Belanja Barang | Admin
@endsection

@section('content')
<div class="card card-body mb-3">
    <div class="row">
        <div class="col-6">
            <h1 class="h3 text-gray-800">Tambah Belanja Barang</h1>

        </div>


    </div>
</div>

<div class="card card-body">
    <div class="row" style="min-height: 60vh">
        <div class="col-12">
            <form action="{{ route('admin.belanja-barang.store') }}" method="POST">
                @csrf
                <div class="row">

                    <div class="col-3">
                        <div class="form-group">
                            <label for="tanggal">Tanggal</label>
                            <input type="text" id="tanggal" class="form-control @error('tanggal') is-invalid @enderror"
                                id="tanggal" name="tanggal" value="{{ old('tanggal') }}">
                            @error('tanggal')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>

                    <div class="col-3">
                        <div class="form-group">
                            <label for="kategori">Kategori</label>
                            <select name="kategori" class="custom-select @error('kategori') is-invalid @enderror"
                                id="kategori">
                                <option>Pilih Kategori</option>
                                <option value="ATK">ATK</option>
                                <option value="Materai">Materai</option>
                                <option value="Alat Kebersihan">Alat Kebersihan</option>
                                <option value="Alat Listrik">Alat Listrik</option>
                            </select>
                            @error('kategori')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label>Pilih Suplier</label>
                            <select class="custom-select @error('suplier_id') is-invalid @enderror" id="select2"
                                name="suplier_id">
                                <option selected></option>
                                @foreach ($suplier as $item)
                                <option value="{{ $item->id_suplier }}">{{ $item->nama_suplier }}</option>
                                @endforeach

                            </select>
                            @error('suplier_id')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>


                    <div class="col-2" style="height: 100%; padding-top: 30px">
                        <a href="#" class="btn btn-success float-right" data-toggle="modal"
                            data-target="#suplierModal">Tambah Suplier</a>
                    </div>
                </div>

                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="pegawai">Pegawai</label>
                            <select name="pegawai_id" class="custom-select  @error('pegawai_id') is-invalid @enderror"
                                id="pegawai">
                                <option>Pilih Pegawai</option>

                                @foreach ($pegawai as $p)
                                <option value="{{ $p->id_pegawai }}">{{ $p->nama_pegawai }}</option>
                                @endforeach

                            </select>
                            @error('pegawai_id')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                </div>

                <hr>

                <div class="row">
                    <div class="col-12">
                        <div class="d-flex justify-content-between">
                            <h5>Detail Belanja</h5>

                            <a href="#" class="btn btn-sm btn-success" id="btnTambah">
                                <i class="fas fa-plus"></i> Tambah Barang
                            </a>
                        </div>

                    </div>
                </div>

                <div class="box" id="wrapper-detail" style="">

                    <div class="row" style="width: 100%">
                        <div class="col-4">
                            <div class="form-group">
                                <label>Nama Barang</label>
                                <input type="text" name="nama_barang[]" class="form-control " id="nama_barang"
                                    placeholder="Nama Barang ...">
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <label>Banyaknya</label>
                                <input type="number" name="qty[]" class="form-control " id="qty"
                                    placeholder="Banyaknya ...">
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <label>Satuan</label>
                                <input type="text" name="satuan[]" class="form-control " id="satuan" value="Buah"
                                    placeholder="Satuan ...">
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                <label>Harga Satuan</label>
                                <input type="number" name="harga_satuan[]" class="form-control " id="harga_satuan"
                                    placeholder="Harga Satuan ...">
                            </div>
                        </div>
                    </div>

                </div>
                <button type="submit" class="btn btn-success float-right mt-5">Tambah</button>
            </form>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="suplierModal" tabindex="-1" aria-labelledby="suplierModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="suplierModalLabel">Tambah Suplier</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('admin.suplier.store') }}" method="POST" id="storeSuplier">
                    @csrf
                    <input type="hidden" name="quickStore" value="1">
                    <div class="form-group">
                        <label for="">Nama Suplier</label>
                        <input type="text" class="form-control" name="nama_suplier" id="nama_suplier">
                    </div>
                    <div class="form-group">
                        <label for="">Kota</label>
                        <input type="text" class="form-control" name="kota" id="kota">
                    </div>
                    <div class="form-group">
                        <label for="">Alamat</label>
                        <textarea class="form-control" name="alamat" id="alamat" cols="30" rows="4"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="">No Telp</label>
                        <input type="text" class="form-control" name="nohp" id="nohp">
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-success" form="storeSuplier">Tambah Suplier</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('tambahStyle')
<link rel="stylesheet" href="{{ asset('backend/vendor/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('backend/vendor/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endpush

@push('tambahScript')
<script src="{{ asset('backend/vendor/select2/js/select2.full.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://npmcdn.com/flatpickr/dist/l10n/id.js"></script>

<script>
    $('#select2, #kategori, #pegawai').select2({
        placeholder: "Pilih ...",
        theme: 'bootstrap4',
    });

    flatpickr.localize(flatpickr.l10ns.id);
    $('#tanggal').flatpickr({
        allowInput:true,
        altInput: true,
        altFormat: "j F Y",
        dateFormat: "Y-m-d",
    });

    $(document).ready(function() {
        var max_fields      = 10;
        var wrapper   		= $("#wrapper-detail");
        var add_button      = $("#btnTambah");

        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append(`
                        <div class="row" style="width: 100%">
                            <div class="col-4">
                                <div class="form-group">
                                    <label>Nama Barang</label>
                                    <input type="text" name="nama_barang[]" class="form-control " id="nama_barang"
                                        placeholder="Nama Barang ...">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="form-group">
                                    <label>Banyaknya</label>
                                    <input type="number" name="qty[]" class="form-control " id="qty"
                                        placeholder="Banyaknya ...">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="form-group">
                                    <label>Satuan</label>
                                    <input type="text" name="satuan[]" class="form-control " id="satuan" value="Buah"
                                        placeholder="Satuan ...">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label>Harga Satuan</label>
                                    <input type="number" name="harga_satuan[]" class="form-control " id="harga_satuan"
                                        placeholder="Harga Satuan ...">
                                </div>
                            </div>
                            <div class="col-1 d-flex align-items-center pt-2">
                                <a href="#" class="btn btn-danger remove_field">Hapus</a>
                            </div>
                        </div>
            `); //add input box
            }
        });

        $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
            e.preventDefault(); $(this).closest('.row').remove(); x--;
        })
    });
</script>
@endpush
