@extends('layouts.backend')

@section('title')
Belanja Barang | Admin
@endsection

@section('content')
<div class="card card-body mb-3">
    <div class="row">
        <div class="col-6">
            <h1 class="h3 text-gray-800">Belanja Barang</h1>

        </div>

        <div class="col-6">
            <div class=" d-flex justify-content-end">

                <a href="{{ route('admin.belanja-barang.printRekapBelanja') }}" target="_blank"
                    class="btn btn-info btn-sm mr-2">
                    <i class="fa fa-print" aria-hidden="true"></i> Print Rekap Belanja Barang
                </a>
                <a href="{{ route('admin.belanja-barang.create') }}" class="btn btn-success btn-sm ">
                    <i class="fas fa-plus"></i> Tambah Data Belanja Barang </a>
            </div>
        </div>
    </div>
</div>

<div class="card card-body">
    <div class="row">
        <div class="col-12">
            <div class="table-responsive">
                <table class="table table-striped table-bordered" id="table" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th style="width: 20px">No</th>
                            <th>Tanggal</th>
                            <th>Kategori</th>
                            <th>Suplier</th>
                            <th>Total Belanja</th>
                            <th>Pegawai</th>
                            <th style="width: 100px">Aksi</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection


@push('tambahStyle')
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css" rel="stylesheet">
@endpush

@push('tambahScript')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>

<script type="text/javascript">
    function htmlDecode(data){
    var txt=document.createElement('textarea');
    txt.innerHTML=data;
    return txt.value
  }

  $(document).ready(function() {
    $('#table').DataTable({
      language:{
            "url":"//cdn.datatables.net/plug-ins/1.10.21/i18n/Indonesian.json",
            "sEmptyTable":"Tidak Ada Data"
      },
      processing:true,
      serverside:true,
      ajax: "{{ route('admin.belanja-barang.getBelanjaBarang') }}",
      columns:[
        {data: 'DT_RowIndex', name: 'DT_RowIndex'},
        {data: 'tanggal', name: 'tanggal'},
        {data: 'kategori', name: 'kategori'},
        {data: 'suplier.nama_suplier', name: 'suplier.nama_suplier'},
        {data: 'total_belanja', name: 'total_belanja'},
        {data: 'pegawai.nama_pegawai', name: 'pegawai.nama_pegawai'},
        {data: "aksi",
              render: function(data){
                return htmlDecode(data);
              }, orderable: false, searchable: false
            }
      ]
    });

  } );
</script>
@endpush
