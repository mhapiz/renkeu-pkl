@extends('layouts.backend')

@section('title')
Suplier | Admin
@endsection

@section('content')
<div class="card card-body mb-3">
    <div class="row">
        <div class="col-6">
            <h1 class="h3 text-gray-800">Edit Suplier</h1>

        </div>


    </div>
</div>

<div class="card card-body">
    <div class="row">
        <div class="col-12">
            <form action="{{ route('admin.suplier.update', $data->id_suplier) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label class="form-label">Nama Suplier</label>
                            <input type="text" class="form-control @error('nama_suplier') is-invalid @enderror"
                                name="nama_suplier" value="{{ $data->nama_suplier }}">
                            @error('nama_suplier')
                            <div class="invalid-feedback text-capitalize">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label class="form-label">Kota</label>
                            <input type="text" class="form-control @error('kota') is-invalid @enderror" name="kota"
                                value="{{ $data->kota }}">
                            @error('kota')
                            <div class="invalid-feedback text-capitalize">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label class="form-label">Alamat</label>
                            <textarea name="alamat" id="" rows="10"
                                class="form-control @error('alamat') is-invalid @enderror">{{ $data->alamat }}</textarea>
                            @error('alamat')
                            <div class="invalid-feedback text-capitalize">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label class="form-label">No. Hp</label>
                            <input type="text" class="form-control @error('nohp') is-invalid @enderror" name="nohp"
                                value="{{ $data->nohp }}">
                            @error('nohp')
                            <div class="invalid-feedback text-capitalize">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>

                </div>
                <button type="submit" class="btn btn-primary">Perbarui</button>
            </form>
        </div>
    </div>
</div>
@endsection


@push('tambahStyle')
@endpush

@push('tambahScript')
@endpush
