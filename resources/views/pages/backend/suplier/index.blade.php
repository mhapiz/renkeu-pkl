@extends('layouts.backend')

@section('title')
Suplier | Admin
@endsection

@section('content')
<div class="card card-body mb-3">
    <div class="row">
        <div class="col-6">
            <h1 class="h3 text-gray-800">Suplier</h1>

        </div>

        <div class="col-6">
            <a href="{{ route('admin.suplier.create') }}" class="btn btn-success btn-sm float-right">
                <i class="fas fa-plus"></i> Tambah Data Suplier </a>
        </div>
    </div>
</div>

<div class="card card-body">
    <div class="row">
        <div class="col-12">
            <div class="table-responsive">
                <table class="table table-striped table-bordered" id="table" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th style="width: 20px">No</th>
                            <th>Nama Suplier</th>
                            <th>Kota</th>
                            <th>Alamat</th>
                            <th>No. Hp</th>
                            <th style="width: 100px">Aksi</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection


@push('tambahStyle')
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css" rel="stylesheet">
@endpush

@push('tambahScript')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>

<script type="text/javascript">
    function htmlDecode(data){
    var txt=document.createElement('textarea');
    txt.innerHTML=data;
    return txt.value
  }

  $(document).ready(function() {
    $('#table').DataTable({
      language:{
            "url":"//cdn.datatables.net/plug-ins/1.10.21/i18n/Indonesian.json",
            "sEmptyTable":"Tidak Ada Data"
      },
      processing:true,
      serverside:true,
      ajax: "{{ route('admin.suplier.getSuplier') }}",
      columns:[
        {data: 'DT_RowIndex', name: 'DT_RowIndex'},
        {data: 'nama_suplier', name: 'nama_suplier'},
        {data: 'kota', name: 'kota'},
        {data: 'alamat', name: 'alamat'},
        {data: 'nohp', name: 'nohp'},
        {data: "aksi",
              render: function(data){
                return htmlDecode(data);
              }, orderable: false, searchable: false
            }
      ]
    });

  } );
</script>
@endpush
