<div class="modal-body">
    <form action="{{ route('admin.berita-acara-pemeriksaan.update', $baPemeriksaan->id_berita_acara_pemeriksaan) }}"
        method="POST" id="update">
        @csrf
        @method('PUT')
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label for="no_surat">Nomor Surat</label>
                    <input type="text" class="form-control" id="no_surat" name="no_surat"
                        value="{{ $baPemeriksaan->no_surat }}" placeholder="Nomor Surat">
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <label>Data Surat Pengadaan</label>
                    <select class="form-control" id="surat_pengadaan_id" name="surat_pengadaan_id">
                        <option>Pilih Data Surat Pengadaan</option>
                        @php
                        $spe =
                        \App\Models\SuratPengadaan::select(['id_surat_pengadaan','no_surat','tanggal','belanja_barang_id'])->find($baPemeriksaan->surat_pengadaan_id);
                        $bb =
                        \App\Models\BelanjaBarang::select(['id_belanja_barang','kategori'])->find($spe->belanja_barang_id);
                        @endphp
                        <option value="{{ $baPemeriksaan->surat_pengadaan_id }}" selected>
                            01 / {{ str_pad($spe->no_surat, 3, '0', STR_PAD_LEFT) }} / {{
                            $bb->kategori }} / KEC. MTP --
                            {{ Carbon\Carbon::parse($spe->tanggal)->isoFormat('dddd, D MMMM Y') }}
                        </option>
                        @foreach ($suratPengadaan as $sp)
                        <option value="{{ $sp->id_surat_pengadaan }}">
                            01 / {{ str_pad($sp->no_surat, 3, '0', STR_PAD_LEFT) }} / {{
                            $sp->belanjaBarang->kategori }} / KEC. MTP --
                            {{ Carbon\Carbon::parse($sp->tanggal)->isoFormat('dddd, D MMMM Y') }}
                        </option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
    <button type="submit" form="update" class="btn btn-info">Update</button>
</div>
