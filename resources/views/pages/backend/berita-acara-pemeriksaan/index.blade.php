@extends('layouts.backend')

@section('title')
Berita Acara Pemeriksaan | Admin
@endsection

@section('content')
<div class="card card-body mb-3">
    <div class="row">
        <div class="col-6">
            <h1 class="h3 text-gray-800">Berita Acara Pemeriksaan</h1>
        </div>

        <div class="col-6">

            <div class="d-flex justify-content-end">
                <a href="{{ route('admin.berita-acara-pemeriksaan.printRekapBeritaAcaraPemeriksaan') }}"
                    class="btn btn-info btn-sm btn-info mr-2" target="_blank">
                    <i class="fa fa-print" aria-hidden="true"></i>
                    Print Rekap Berita Acara Pemeriksaan
                </a>

                <button type="button" class="btn btn-primary btn-sm float-right" data-toggle="modal"
                    data-target="#tambahModal">
                    <i class="fas fa-plus"></i>
                    Tambah Berita Acara Pemeriksaan
                </button>
            </div>


        </div>
    </div>
</div>

<div class="card card-body">
    <div class="row">
        <div class="col-12">
            <div class="table-responsive">
                <table class="table table-striped table-bordered" id="table" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th style="width: 20px">No</th>
                            <th>Nomor Berita Acara Pemeriksaan</th>
                            <th>Nomor Surat Pengadaan</th>
                            <th>Belanja Barang</th>
                            <th>Total Belanja</th>
                            <th style="width: 100px">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($baPemeriksaan as $ba)

                        @php
                        $belanjaBarang =
                        App\Models\BelanjaBarang::where('id_belanja_barang','=',$ba->suratPengadaan->belanja_barang_id)->first();
                        @endphp
                        <tr>
                            <td>{{ $loop->iteration }}</td>

                            <td>
                                027 / {{ str_pad($ba->no_surat, 3, '0', STR_PAD_LEFT) }} /
                                {{ $belanjaBarang->kategori }}
                                / KEC. MTP
                            </td>

                            <td>
                                01 / {{ str_pad($ba->suratPengadaan->no_surat, 3, '0', STR_PAD_LEFT) }} /
                                {{ $belanjaBarang->kategori }}
                                / KEC. MTP
                            </td>

                            <td>Belanja <span class="font-weight-bold">{{ $belanjaBarang->kategori }}</span>,
                                Tanggal {{ Carbon\Carbon::parse($belanjaBarang->tanggal)->isoformat('D MMMM Y') }}
                            </td>

                            <td>
                                @php
                                $detailBelanja = App\Models\DetailBelanjaBarang::where('belanja_barang_id',
                                $ba->suratPengadaan->belanja_barang_id)->get();
                                $total = 0;
                                foreach ($detailBelanja as $db) {
                                $total += $db->jumlah ;
                                }
                                @endphp
                                Rp {{ number_format($total,0,',','.') }}
                            </td>

                            <td>
                                <div class="btn-group" role="group" aria-label="Basic example">

                                    <button class="btn btn-warning btn-edit text-white" data-toggle="modal"
                                        data-target="#editModal" id="getBAPemeriksaan"
                                        data-url="{{ route('admin.berita-acara-pemeriksaan.edit',$ba->id_berita_acara_pemeriksaan) }}">
                                        <i class="fas fa-edit"></i>
                                    </button>

                                    <a class="btn btn-info btn-print text-white"
                                        href="{{ route('admin.berita-acara-pemeriksaan.print',$ba->id_berita_acara_pemeriksaan) }}"
                                        target="_blank">
                                        <i class="fa fa-print" aria-hidden="true"></i>
                                    </a>

                                    <form
                                        action="{{ route('admin.berita-acara-pemeriksaan.delete', $ba->id_berita_acara_pemeriksaan) }}"
                                        method="POST">
                                        @csrf
                                        @method('delete')
                                        <button class="btn btn-danger btn-delete rounded-right"
                                            style="border-bottom-left-radius: 0px; border-top-left-radius: 0px; "
                                            onclick="return confirm('Apakah Anda Yakin Ingin Menghapus Data Ini?')">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </form>

                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="tambahModal" tabindex="-1" aria-labelledby="tambahModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="tambahModalLabel">Tambah Berita Acara Pemeriksaan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('admin.berita-acara-pemeriksaan.store') }}" method="POST" id="tambahSurat">
                    @csrf
                    @php
                    if
                    (App\Models\BeritaAcaraPemeriksaan::whereYear('created_at','=',Carbon\Carbon::now()->format('Y'))->orderBy('created_at',
                    'desc')->first()) {

                    $no_surat =
                    App\Models\BeritaAcaraPemeriksaan::whereYear('created_at','=',Carbon\Carbon::now()->format('Y'))->orderBy('created_at',
                    'desc')->first()->no_surat + 1;


                    } else{
                    $no_surat = 1;
                    }
                    @endphp
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="no_surat">Nomor Surat</label>
                                <input type="text" class="form-control" id="no_surat" name="no_surat"
                                    value="{{ $no_surat }}" placeholder="Nomor Surat">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Data Surat Pengadaan</label>
                                <select class="form-control" id="surat_pengadaan_id" name="surat_pengadaan_id">
                                    <option>Pilih Data Surat Pengadaan</option>
                                    @foreach ($suratPengadaan as $sp)
                                    <option value="{{ $sp->id_surat_pengadaan }}">
                                        01 / {{ str_pad($sp->no_surat, 3, '0', STR_PAD_LEFT) }} / {{
                                        $sp->belanjaBarang->kategori }} / KEC. MTP --
                                        {{ Carbon\Carbon::parse($sp->tanggal)->isoFormat('dddd, D MMMM Y') }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="submit" form="tambahSurat" class="btn btn-primary">Tambah</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="editModal" tabindex="-1" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editModalLabel">Edit Berita Acara Pemeriksaan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="dynamic-content"></div>
        </div>
    </div>
</div>
@endsection


@push('tambahStyle')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css" rel="stylesheet">
@endpush

@push('tambahScript')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://npmcdn.com/flatpickr/dist/l10n/id.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>

<script>
    flatpickr.localize(flatpickr.l10ns.id);
    $('#tanggal').flatpickr({
        allowInput:true,
        altInput: true,
        altFormat: "j F Y",
        dateFormat: "Y-m-d",
    });

    $(document).ready(function() {
        $('#table').DataTable();

        $(document).on('click', '#getBAPemeriksaan', function(e) {
            e.preventDefault();
            var url = $(this).data('url');
            console.log(url);
            $.ajax({
                url: url,
                method: "get",
                dataType: "html",
                }).done(function(data) {
                    $('#dynamic-content').html(data);
                }).fail(function(error) {
                    $('#dynamic-content').html(`
                    <div class="modal-body">
                        <h2>Maaf, terjadi kesalahan</h2>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-secondary" disabled>Update</button>
                    </div>

                    `);
                });
        });
    });
</script>
@endpush
