@extends('layouts.backend')

@section('title')
Dashboard | Admin
@endsection

@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-4 text-gray-800">Dashboard</h1>

<h5>Data Master</h5>
<div class="row">

    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-secondary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2 py-0">
                        <a href="{{ route('admin.pegawai.index') }}"
                            class="stretched-link text-xs font-weight-bold text-primary text-uppercase mb-1">
                            Data Pegawai</a>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $jumlahPegawai }} Data</div>

                    </div>
                    <div class="col-auto">
                        <i class="fa fa-users fa-2x text-gray-300" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-secondary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <a href="{{ route('admin.suplier.index') }}"
                            class="stretched-link text-xs font-weight-bold text-primary text-uppercase mb-1">
                            Data Suplier</a>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $jumlahSuplier }} Data</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-warehouse fa-2x text-gray-300 "></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-secondary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <a href="{{ route('admin.belanja-barang.index') }}"
                            class="stretched-link text-xs font-weight-bold text-primary text-uppercase mb-1">
                            Data Belanja Barang</a>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $jumlahBelanjaBarang }} Data</div>
                    </div>
                    <div class="col-auto">
                        <i class="fa fa-list fa-2x text-gray-300" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>


<h5>Laporan</h5>
<div class="row">

    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-secondary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <a href="{{ route('admin.surat-pengadaan.index') }}"
                            class="stretched-link text-xs font-weight-bold text-primary text-uppercase mb-1">
                            Surat Pengadaan</a>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $jumlahSuratPengadaan }} Data</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-file-alt fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-secondary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <a href="{{ route('admin.berita-acara-pemeriksaan.index') }}"
                            class="stretched-link text-xs font-weight-bold text-primary text-uppercase mb-1">
                            Berita Acara Pemeriksaan</a>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $jumlahBaPemeriksaan }} Data</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-file-alt fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-secondary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <a href="{{ route('admin.berita-acara-serah-terima.index') }}"
                            class="stretched-link text-xs font-weight-bold text-primary text-uppercase mb-1">
                            Berita Acara Serah Terima</a>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $jumlahBaSerahTerima }} Data</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-file-alt fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>

@endsection
