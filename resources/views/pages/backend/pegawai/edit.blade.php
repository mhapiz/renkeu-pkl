@extends('layouts.backend')

@section('title')
Pegawai | Admin
@endsection

@section('content')
<div class="card card-body mb-3">
    <div class="row">
        <div class="col-6">
            <h1 class="h3 text-gray-800">Edit Pegawai</h1>

        </div>


    </div>
</div>

<div class="card card-body">
    <div class="row">
        <div class="col-12">
            <form action="{{ route('admin.pegawai.update', $data->id_pegawai) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label class="form-label">NIP</label>
                            <input type="text" class="form-control @error('nip') is-invalid @enderror" name="nip"
                                value="{{ $data->nip }}">
                            @error('nip')
                            <div class="invalid-feedback text-capitalize">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label class="form-label">Nama Pegawai</label>
                            <input type="text" class="form-control @error('nama_pegawai') is-invalid @enderror"
                                name="nama_pegawai" value="{{ $data->nama_pegawai }}">
                            @error('nama_pegawai')
                            <div class="invalid-feedback text-capitalize">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label class="form-label">Jabatan</label>
                            <input type="text" class="form-control @error('jabatan') is-invalid @enderror"
                                name="jabatan" value="{{ $data->jabatan }}">
                            @error('jabatan')
                            <div class="invalid-feedback text-capitalize">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label class="form-label">No. Hp</label>
                            <input type="text" class="form-control @error('nohp') is-invalid @enderror" name="nohp"
                                value="{{ $data->nohp }}">
                            @error('nohp')
                            <div class="invalid-feedback text-capitalize">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label class="form-label">Alamat</label>
                            <textarea name="alamat" id="" rows="10"
                                class="form-control @error('alamat') is-invalid @enderror">{{ $data->alamat }}</textarea>
                            @error('alamat')
                            <div class="invalid-feedback text-capitalize">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection


@push('tambahStyle')
@endpush

@push('tambahScript')
@endpush
