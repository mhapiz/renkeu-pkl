<div class="modal-body">
    <form action="{{ route('admin.berita-acara-serah-terima.update', $baSerahTerima) }}" method="POST" id="update">
        @csrf
        @method('PUT')
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label for="no_surat">Nomor Surat</label>
                    <input type="text" class="form-control" id="no_surat" name="no_surat"
                        value="{{ $baSerahTerima->no_surat }}" placeholder="Nomor Surat">
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <label>Data Berita Acara Pemeriksaan</label>
                    <select class="form-control" id="berita_acara_pemeriksaan_id" name="berita_acara_pemeriksaan_id">
                        <option>Pilih Data Berita Acara Pemeriksaan</option>
                        @php
                        $bpe =
                        App\Models\BeritaAcaraPemeriksaan::where('id_berita_acara_pemeriksaan','=',$baSerahTerima->berita_acara_pemeriksaan_id)->first();

                        $spe =
                        App\Models\SuratPengadaan::where('id_surat_pengadaan','=',$bpe->surat_pengadaan_id)->first();

                        $bbe =
                        App\Models\BelanjaBarang::where('id_belanja_barang','=',$spe->belanja_barang_id)->first();

                        @endphp

                        <option value="{{ $baSerahTerima->berita_acara_pemeriksaan_id }}" selected>
                            027 / {{ str_pad($bpe->no_surat, 3, '0', STR_PAD_LEFT) }} / {{
                            $bbe->kategori }} / KEC. MTP --
                            {{ Carbon\Carbon::parse($baSerahTerima->tanggal)->isoFormat('dddd, D MMMM Y') }}
                        </option>

                        @foreach ($baPemeriksaan as $bp)
                        @php
                        $sp =
                        App\Models\SuratPengadaan::where('id_surat_pengadaan','=',$bp->surat_pengadaan_id)->first();

                        $bb =
                        App\Models\BelanjaBarang::where('id_belanja_barang','=',$sp->belanja_barang_id)->first();

                        @endphp

                        <option value="{{ $bp->id_berita_acara_pemeriksaan }}">
                            027 / {{ str_pad($bp->no_surat, 3, '0', STR_PAD_LEFT) }} / {{
                            $bb->kategori }} / KEC. MTP --
                            {{ Carbon\Carbon::parse($bp->tanggal)->isoFormat('dddd, D MMMM Y') }}
                        </option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
    <button type="submit" form="update" class="btn btn-info">Update</button>
</div>
