@extends('layouts.backend')

@section('title')
Dashboard | Kasubag
@endsection

@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-4 text-gray-800">Dashboard</h1>

<h5>Laporan</h5>
<div class="row">

    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-secondary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <a href="{{ route('kasubag.surat-pengadaan.index') }}"
                            class="stretched-link text-xs font-weight-bold text-primary text-uppercase mb-1">
                            Surat Pengadaan</a>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $jumlahSuratPengadaan }} Data</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-file-alt fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-secondary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <a href="{{ route('kasubag.berita-acara-pemeriksaan.index') }}"
                            class="stretched-link text-xs font-weight-bold text-primary text-uppercase mb-1">
                            Berita Acara Pemeriksaan</a>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $jumlahBaPemeriksaan }} Data</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-file-alt fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-secondary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <a href="{{ route('kasubag.berita-acara-serah-terima.index') }}"
                            class="stretched-link text-xs font-weight-bold text-primary text-uppercase mb-1">
                            Berita Acara Serah Terima</a>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $jumlahBaSerahTerima }} Data</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-file-alt fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
@endsection
