@extends('layouts.backend')

@section('title')
Berita Acara Pemeriksaan | Kasubag
@endsection

@section('content')
<div class="card card-body mb-3">
    <div class="row">
        <div class="col-6">
            <h1 class="h3 text-gray-800">Berita Acara Pemeriksaan</h1>
        </div>

        <div class="col-6">

            <div class="d-flex justify-content-end">
                <a href="{{ route('kasubag.berita-acara-pemeriksaan.printRekapBeritaAcaraPemeriksaan') }}"
                    class="btn btn-info btn-sm btn-info mr-2" target="_blank">
                    <i class="fa fa-print" aria-hidden="true"></i>
                    Print Rekap Berita Acara Pemeriksaan
                </a>

            </div>


        </div>
    </div>
</div>

<div class="card card-body">
    <div class="row">
        <div class="col-12">
            <div class="table-responsive">
                <table class="table table-striped table-bordered" id="table" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th style="width: 20px">No</th>
                            <th>Nomor Berita Acara Pemeriksaan</th>
                            <th>Nomor Surat Pengadaan</th>
                            <th>Belanja Barang</th>
                            <th>Total Belanja</th>
                            <th style="width: 70px">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($baPemeriksaan as $ba)

                        @php
                        $belanjaBarang =
                        App\Models\BelanjaBarang::where('id_belanja_barang','=',$ba->suratPengadaan->belanja_barang_id)->first();
                        @endphp
                        <tr>
                            <td>{{ $loop->iteration }}</td>

                            <td>
                                027 / {{ str_pad($ba->no_surat, 3, '0', STR_PAD_LEFT) }} /
                                {{ $belanjaBarang->kategori }}
                                / KEC. MTP
                            </td>

                            <td>
                                01 / {{ str_pad($ba->suratPengadaan->no_surat, 3, '0', STR_PAD_LEFT) }} /
                                {{ $belanjaBarang->kategori }}
                                / KEC. MTP
                            </td>

                            <td>Belanja <span class="font-weight-bold">{{ $belanjaBarang->kategori }}</span>,
                                Tanggal {{ Carbon\Carbon::parse($belanjaBarang->tanggal)->isoformat('D MMMM Y') }}
                            </td>

                            <td>
                                @php
                                $detailBelanja = App\Models\DetailBelanjaBarang::where('belanja_barang_id',
                                $ba->suratPengadaan->belanja_barang_id)->get();
                                $total = 0;
                                foreach ($detailBelanja as $db) {
                                $total += $db->jumlah ;
                                }
                                @endphp
                                Rp {{ number_format($total,0,',','.') }}
                            </td>

                            <td>
                                <a class="btn btn-info btn-print text-white btn-sm"
                                    href="{{ route('kasubag.berita-acara-pemeriksaan.print',$ba->id_berita_acara_pemeriksaan) }}"
                                    target="_blank">
                                    <i class="fa fa-print" aria-hidden="true"></i> Print
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection


@push('tambahStyle')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css" rel="stylesheet">
@endpush

@push('tambahScript')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://npmcdn.com/flatpickr/dist/l10n/id.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>

<script>
    flatpickr.localize(flatpickr.l10ns.id);
    $('#tanggal').flatpickr({
        allowInput:true,
        altInput: true,
        altFormat: "j F Y",
        dateFormat: "Y-m-d",
    });

    $(document).ready(function() {
        $('#table').DataTable();

    });
</script>
@endpush
