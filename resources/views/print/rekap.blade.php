<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>print</title>
    <style>
        body {
            margin: 5px 10px;
            padding: 0;
            box-sizing: border-box;
            font-family: 'Times New Roman', Times, serif;
            font-size: 14px;
        }

        .w-full {
            width: 100%;
        }

        p {
            margin: 0
        }

        .page-break {
            page-break-after: always;
        }

        .va-top {
            vertical-align: top
        }

        .fw-bold {
            font-weight: 700;
        }

        .text-center {
            text-align: center
        }

        .table-special {
            border-collapse: collapse;
            border: 1px solid black;
            width: 100%;
        }

        .table-special td {
            border-right: 1px solid black;
            padding: 2px 12px;

        }

        .table {
            border-collapse: collapse
        }

        .table-bordered td,
        th {
            border: 1px solid black;
            padding: 8px 12px;
        }



        .table-pad td,
        th {
            padding: 5px 12px;
        }

        p {
            text-align: justify;
        }
    </style>
</head>

<body>
    <div class="header">
        <table class="w-full " style=" height: 100px; border-bottom: 1px double black; padding-bottom: 10px">
            <tr>
                <td style="width: 10%">
                    <img src="{{ asset('assets/banjar.png') }}" alt="logo" style="height: 100%">
                </td>
                <td style="width: auto; text-align: center !important;font-size: 1.5rem;padding-left: 20px ">
                    <p>
                        PEMERINTAH KABUPATEN BANJAR
                    </p>

                    <p style="font-size: 2.1rem; font-weight: bold">
                        KECAMATAN MARTAPURA
                    </p>
                    <p style="font-size: 1.1rem" class="fw-bold">
                        Jl.Sekumpul Ujung No.1 Bincau kode Pos 70651
                    </p>

                </td>
                <td style="width: 10%">
                    <img src="{{ asset('assets/banjar.png') }}" alt="logo" style="height: 100% ;opacity: 0;">
                </td>
            </tr>

        </table>
    </div>

    <div class="body" style="margin-top: 10px; ">

        <div class="text-center" style="font-size: 1.5rem; font-weight: bold; margin: 10px 0">
            Rekap Data Belanja
        </div>

        <table class="table table-bordered w-full">
            <thead>
                <tr>
                    <th style="width: 30px">No.</th>
                    <th>Kategori</th>
                    <th>Tanggal</th>
                    <th>Suplier</th>
                    <th>Pegawai</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1.</td>
                    <td>ATK</td>
                    <td>Kamis, 23 Desember 2021</td>
                    <td>Fotocopy Berkat Guru</td>
                    <td>Hapizzz</td>
                </tr>
                <tr>
                    <td>2.</td>
                    <td>ATK</td>
                    <td>Kamis, 23 Desember 2021</td>
                    <td>Fotocopy Berkat Guru</td>
                    <td>Hapizzz</td>
                </tr>
                <tr>
                    <td>3.</td>
                    <td>ATK</td>
                    <td>Kamis, 23 Desember 2021</td>
                    <td>Fotocopy Berkat Guru</td>
                    <td>Hapizzz</td>
                </tr>
                <tr>
                    <td>4.</td>
                    <td>ATK</td>
                    <td>Kamis, 23 Desember 2021</td>
                    <td>Fotocopy Berkat Guru</td>
                    <td>Hapizzz</td>
                </tr>
            </tbody>
        </table>

        <br>

        <table class="w-full table " style="margin: 20px 0; font-size: .8rem">

            <tr>
                <td></td>
                <td style="width: 40% !important"></td>
                <td class="text-center">Pejabat Pembuat Teknis Kebijakan</td>
            </tr>


            <tr>
                <td> <br><br><br><br> </td>
                <td style="width: 40% !important"> <br><br><br><br> </td>
                <td> <br><br><br><br> </td>
            </tr>

            <tr>
                <td></td>

                <td style="width: 40% !important"></td>
                <td class="text-center fw-bold">
                    <u>M. Rusydi Ansharie </u>
                </td>
            </tr>

            <tr>
                <td></td>
                <td style="width: 40% !important"></td>
                <td class="text-center fw-bold">
                    NIP. 19860804 200803 1 001
                </td>
            </tr>
        </table>

    </div>

</body>
