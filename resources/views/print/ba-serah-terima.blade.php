<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>print</title>
    <style>
        body {
            margin: 5px 20px;
            padding: 0;
            box-sizing: border-box;
            font-family: 'Times New Roman', Times, serif;
            font-size: 14px;
        }

        .w-full {
            width: 100%;
        }

        p {
            margin: 0
        }

        .page-break {
            page-break-after: always;
        }

        .va-top {
            vertical-align: top
        }

        .fw-bold {
            font-weight: 700;
        }

        .text-center {
            text-align: center
        }

        .table-special {
            border-collapse: collapse;
            border: 1px solid black;
            width: 100%;
        }

        .table-special td {
            border-right: 1px solid black;
            padding: 2px 12px;

        }

        .table {
            border-collapse: collapse
        }

        .table-bordered td,
        th {
            border: 1px solid black;
            padding: 8px 12px;
        }



        .table-pad td,
        th {
            padding: 5px 12px;
        }

        p {
            text-align: justify;
        }
    </style>
</head>

<body>

    <div class="header">
        <table class="w-full " style=" height: 100px; border-bottom: 1px double black; padding-bottom: 10px">
            <tr>
                <td style="width: 10%">
                    <img src="{{ asset('assets/banjar.png') }}" alt="logo" style="height: 100%">
                </td>
                <td style="width: auto; text-align: start;font-size: 1.5rem;padding-left: 20px ">
                    <p>
                        PEMERINTAH KABUPATEN BANJAR
                    </p>

                    <p style="font-size: 2.1rem; font-weight: bold">
                        KECAMATAN MARTAPURA
                    </p>
                    <p style="font-size: 1.1rem" class="fw-bold">
                        Jl.Sekumpul Ujung No.1 Bincau kode Pos 70651
                    </p>

                </td>
            </tr>

        </table>
    </div>

    <div class="body" style="margin-top: 10px; ">

        <table class=" w-full">
            <tr>
                <td class="text-center fw-bold">
                    <span style="font-size: 1.1rem">BERITA ACARA SERAH TERIMA BARANG</span> <br>
                    Nomor : 027 / {{ str_pad($baSerahTerima->no_surat, 3, '0', STR_PAD_LEFT) }} /
                    {{ $belanjaBarang->kategori }} / KEC.MTP /
                    {{ Carbon\Carbon::parse($belanjaBarang->tanggal)->isoFormat('Y') }}
                </td>
            </tr>
        </table>

        <table class="w-full">
            <tr>
                <td>
                    <p>
                        &nbsp;&nbsp;&nbsp; &nbsp;&nbsp; Pada hari <span class="fw-bold">{{ $tgl['hari'] }}</span>
                        tanggal <span class="fw-bold">{{ $tgl['tanggal'] }}</span> bulan <span class="fw-bold">{{
                            $tgl['bulan'] }}</span> tahun <span class="fw-bold">{{ $tgl['tahun'] }}</span>, kami yang
                        bertanda tangan di bawah ini
                    </p>
                </td>
            </tr>
        </table>


        <table class="w-full">
            <tr>
                <td class="va-top" style="padding-top: 4px">1</td>
                <td>
                    <table>
                        <tr>
                            <td class="va-top" style="width: 80px">Nama</td>
                            <td class="va-top" style="width: 10px; text-align: center">:</td>
                            <td class="va-top" style="width: auto">M. RUSYDI ANSHARIE</td>
                        </tr>
                        <tr>
                            <td class="va-top" style="width: 80px">NIP</td>
                            <td class="va-top" style="width: 10px; text-align: center">:</td>
                            <td class="va-top" style="width: auto">19860804 200803 1 001</td>
                        </tr>
                        <tr>
                            <td class="va-top" style="width: 80px">Jabatan</td>
                            <td class="va-top" style="width: 10px; text-align: center">:</td>
                            <td class="va-top" style="width: auto">PPTK Kecamatan Martapura</td>
                        </tr>
                        <tr>
                            <td class="va-top" style="width: 80px">Alamat</td>
                            <td class="va-top" style="width: 10px; text-align: center">:</td>
                            <td class="va-top" style="width: auto">
                                Jl. Sekumpul Ujung Bincau No. 1 Martapura kode Pos 70651
                            </td>
                        </tr>
                        <tr>
                            <td class="va-top" colspan="3">Dalam hal ini disebut <b>PIHAK PERTAMA</b></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="va-top" style="padding-top: 4px">2</td>
                <td>
                    <table>
                        <tr>
                            <td class="va-top" style="width: 80px">Nama</td>
                            <td class="va-top" style="width: 10px; text-align: center">:</td>
                            <td class="va-top" style="width: auto">YULIDA MIDAYANI, A.Md</td>
                        </tr>
                        <tr>
                            <td class="va-top" style="width: 80px">NIP</td>
                            <td class="va-top" style="width: 10px; text-align: center">:</td>
                            <td class="va-top" style="width: auto">19870721 200904 2 002</td>
                        </tr>
                        <tr>
                            <td class="va-top" style="width: 80px">Jabatan</td>
                            <td class="va-top" style="width: 10px; text-align: center">:</td>
                            <td class="va-top" style="width: auto">Pengurus Barang Kecamatan Martapura Kab. Banjar</td>
                        </tr>
                        <tr>
                            <td class="va-top" style="width: 80px">Alamat</td>
                            <td class="va-top" style="width: 10px; text-align: center">:</td>
                            <td class="va-top" style="width: auto">
                                Jl. Sekumpul Ujung Bincau No. 1 Martapura kode Pos 70651
                            </td>
                        </tr>
                        <tr>
                            <td class="va-top" colspan="3">Dalam hal ini disebut <b>PIHAK KEDUA</b></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        <br>

        <table class="w-full">
            <tr>
                <td>
                    <p>
                        Dengan ini kedua belah pihak mengadakan serah terima {{ $belanjaBarang->kategori }} sebagai
                        berikut :
                    </p>
                </td>
            </tr>
        </table>


        <table class=" table-special" style="width: 80%">
            @foreach ($belanjaBarang->detail as $d)
            <tr>
                <td style="width: 20px">{{ $loop->iteration }}.</td>
                <td>{{ $d->nama_barang }}</td>
                <td>{{ $d->qty }} {{ $d->satuan }}</td>
            </tr>
            @endforeach
        </table>


        <table class="w-full">
            <tr>
                <td>
                    <p>
                        &nbsp;&nbsp;&nbsp; &nbsp;&nbsp; Demikian Berita Acara ini dibuat untuk diketahui dan
                        dipergunakan sebagaimana mestinya.
                    </p>
                </td>
            </tr>
        </table>

        <br>

        <table class="w-full table " style="margin: 20px 0; font-size: .8rem">

            <tr>
                <td>Yang Menyerahkan</td>
                <td style="width: 40% !important"></td>
                <td>Yang Menerima</td>
            </tr>

            <tr>
                <td>PIHAK PERTAMA</td>
                <td style="width: 40% !important"></td>
                <td>PIHAK KEDUA</td>
            </tr>

            <tr>
                <td> <br><br> </td>
                <td style="width: 40% !important"> <br><br> </td>
                <td> <br><br> </td>
            </tr>

            <tr>
                <td class="fw-bold">
                    <u>M. Rusydi Ansharie</u>
                </td>
                <td style="width: 40% !important"></td>
                <td class="fw-bold">
                    <u>YULIDA MIDAYANI,A.Md</u>


                </td>
            </tr>

            <tr>
                <td class="fw-bold">
                    NIP. 19860804 200803 1 001
                </td>
                <td style="width: 40% !important"></td>
                <td class="fw-bold">
                    NIP 19870721 200904 2 002
                </td>
            </tr>
        </table>

        <table class="w-full table text-center" style="margin: 20px 0; font-size: .8rem">
            <tr>
                <td style="width: 30% !important"></td>
                <td>Mengetahui</td>
                <td style="width: 30% !important"></td>
            </tr>
            <tr>
                <td style="width: 30% !important"></td>
                <td>Camat Martapura</td>
                <td style="width: 30% !important"></td>
            </tr>
            <tr>
                <td style="width: 30% !important"></td>
                <td> <br><br> </td>
                <td style="width: 30% !important"></td>
            </tr>
            <tr>
                <td style="width: 30% !important"></td>
                <td class="fw-bold">
                    <u>MUHAMMAD RAMLI, S.IP, M. AP</u>
                </td>
                <td style="width: 30% !important"></td>
            </tr>
            <tr>
                <td style="width: 30% !important"></td>
                <td class="fw-bold">NIP.19740520 199403 1 001</td>
                <td style="width: 30% !important"></td>
            </tr>
        </table>

    </div>


</body>
