<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>print</title>
    <style>
        body {
            margin: 5px 20px;
            padding: 0;
            box-sizing: border-box;
            font-family: 'Times New Roman', Times, serif;
            font-size: 14px;
        }

        .w-full {
            width: 100%;
        }

        p {
            margin: 0
        }

        .page-break {
            page-break-after: always;
        }

        .va-top {
            vertical-align: top
        }

        .fw-bold {
            font-weight: 700;
        }

        .text-center {
            text-align: center
        }

        .table-special {
            border-collapse: collapse;
            border: 1px solid black;
            width: 100%;
        }

        .table-special td {
            border-right: 1px solid black;
            padding: 2px 12px;

        }

        .table {
            border-collapse: collapse
        }

        .table-bordered td,
        th {
            border: 1px solid black;
            padding: 8px 12px;
        }



        .table-pad td,
        th {
            padding: 5px 12px;
        }

        p {
            text-align: justify;
        }
    </style>
</head>

<body>

    <div class="header">
        <table class="w-full " style=" height: 100px; border-bottom: 1px double black; padding-bottom: 10px">
            <tr>
                <td style="width: 10%">
                    <img src="{{ asset('assets/banjar.png') }}" alt="logo" style="height: 100%">
                </td>
                <td style="width: auto; text-align: start;font-size: 1.5rem;padding-left: 20px ">
                    <p>
                        PEMERINTAH KABUPATEN BANJAR
                    </p>

                    <p style="font-size: 2.1rem; font-weight: bold">
                        KECAMATAN MARTAPURA
                    </p>
                    <p style="font-size: 1.1rem" class="fw-bold">
                        Jl.Sekumpul Ujung No.1 Bincau kode Pos 70651
                    </p>

                </td>
            </tr>

        </table>
    </div>

    <div class="body" style="margin-top: 10px; ">

        <table class=" w-full">
            <tr>
                <td class="text-center fw-bold">
                    <u style="font-size: 1.000001rem"> BERITA ACARA PEMERIKSA BARANG/JASA </u> <br>
                    Nomor : 027 / {{ str_pad($baPemeriksaan->no_surat, 3, '0', STR_PAD_LEFT) }} / {{
                    $belanjaBarang->kategori }}
                    / KEC.MTP
                </td>
            </tr>
        </table>

        <br>


        <table class="w-full">
            <tr>
                <td>
                    <p>
                        &nbsp;&nbsp;&nbsp; &nbsp;&nbsp; Pada hari ini <span class="fw-bold">{{ $tgl['hari'] }}</span>
                        tanggal <span class="fw-bold">{{ $tgl['tanggal'] }}</span>
                        bulan
                        <span class="fw-bold">{{ $tgl['bulan'] }}</span> tahun <span class="fw-bold">{{ $tgl['tahun']
                            }}</span>, bertempat di Kantor
                        Kecamatan Martapura Kabupaten
                        Banjar, yang bertanda
                        tangan di bawah ini :
                    </p>
                </td>
            </tr>
        </table>

        <table class="w-full" style="margin: 20px 0">
            <tr>
                <td>1. Nama</td>
                <td>:</td>
                <td>Mas'yani</td>
                <td>:</td>
                <td>Ketua / merangkap anggota</td>
            </tr>
        </table>

        <table class="w-full">
            <tr>
                <td>
                    <p>
                        &nbsp;&nbsp;&nbsp; &nbsp;&nbsp; Berdasarkan Keputusan Camat Martapura Kabupaten Banjar Nomor 13
                        tanggal 28 Pebruari 2019 tentang Pembentukan Panitia Pemeriksaan Barang/Jasa Kantor Kecamatan
                        Martapura Kabupaten Banjar dan masing-masing karena jabatannya, dengan ini menyatakan dengan
                        sebenarnya telah mengadakan dan melaksanakan pemeriksaan terhadap Barang / Jasa berupa Alat
                        Kebersihan yang dipesan dari :
                    </p>
                </td>
            </tr>
        </table>

        <table class="w-full" style="margin: 20px 0">
            <tr>
                <td>Nama Toko</td>
                <td>:</td>
                <td>{{ $belanjaBarang->suplier->nama_suplier }}</td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td>:</td>
                <td>{{ $belanjaBarang->suplier->alamat }}</td>
            </tr>
        </table>

        <table class="w-full">
            <tr>
                <td>
                    <p>
                        Sebagai realisasi Surat Pesanan / Order Nomor: 01 /
                        {{ str_pad($baPemeriksaan->no_surat, 3, '0', STR_PAD_LEFT) }} /
                        {{ $belanjaBarang->kategori }} / KEC.MTP tanggal
                        {{ Carbon\Carbon::parse($baPemeriksaan->suratPengadaan->tanggal)->isoFormat('D MMMM Y') }}
                        dengan jumlah / jenis barang : <b>Terlampir</b>.
                    </p>
                </td>
            </tr>
        </table>

        <table class="w-full" style="margin: 20px 0">
            <tr>
                <td>Hasil pemeriksaan dinyatakan:</td>
            </tr>
            <tr>
                <td>a. Baik</td>
            </tr>
            <tr>
                <td>b. Kurang / Tidak Baik</td>
            </tr>
        </table>

        <table class="w-full">
            <tr>
                <td>
                    <p>
                        Yang selanjutnya akan diserahkan oleh Penyedia Barang/Jasa kepada Pengurus/Bendahara Barang.
                    </p>
                </td>
            </tr>
        </table>

        <br>

        <table class="w-full">
            <tr>
                <td>
                    <p>
                        &nbsp;&nbsp;&nbsp; &nbsp;&nbsp; Demikian Berita Acara ini dibuat dalam rangkap 3 (tiga) untuk
                        dipergunakan sebagaimana mestinya.
                    </p>
                </td>
            </tr>
        </table>

        <br>

        <table class="w-full">
            <tr>
                <td class="text-center">
                    Martapura, {{ Carbon\Carbon::parse($baPemeriksaan->suratPengadaan->tanggal)->isoFormat('D MMMM Y')
                    }}
                </td>
            </tr>
        </table>
        <table class="w-full">
            <tr>
                <td class="text-center fw-bold">
                    PANITIA PEMERIKSAAN BARANG / JASA
                </td>
            </tr>
        </table>

        <table class="w-full" style="margin-top: 20px ">
            <tr>
                <td>1. Mas'yani</td>
                <td>:</td>
                <td>1 ...............................................................</td>
            </tr>
        </table>

    </div>

    <div class="page-break"></div>

    <div class="header">
        <table class="w-full " style=" height: 100px; border-bottom: 1px double black; padding-bottom: 10px">
            <tr>
                <td style="width: 10%">
                    <img src="{{ asset('assets/banjar.png') }}" alt="logo" style="height: 100%">
                </td>
                <td style="width: auto; text-align: start;font-size: 1.5rem;padding-left: 20px ">
                    <p>
                        PEMERINTAH KABUPATEN BANJAR
                    </p>

                    <p style="font-size: 2.1rem; font-weight: bold">
                        KECAMATAN MARTAPURA
                    </p>
                    <p style="font-size: 1.1rem" class="fw-bold">
                        Jl.Sekumpul Ujung No.1 Bincau kode Pos 70651
                    </p>

                </td>
            </tr>

        </table>
    </div>

    <div class="body" style="margin-top: 10px">

        <table class="w-full">
            <tr>
                <td style="width: 50%"></td>
                <td style="width: 50%">
                    Daftar Lampiran Berita Acara
                </td>
            </tr>
            <tr>
                <td style="width: 50%"></td>
                <td style="width: 50%">
                    Serah terima hasil pekerjaan
                </td>
            </tr>
            <tr>
                <td style="width: 50%"></td>
                <td style="width: 50%">
                    Nomor : 027/ {{ str_pad($baPemeriksaan->no_surat, 3, '0', STR_PAD_LEFT) }}-01/KEC.MTP
                </td>
            </tr>
            <tr>
                <td style="width: 50%"></td>
                <td style="width: 50%">
                    Tanggal : {{ Carbon\Carbon::parse($baPemeriksaan->suratPengadaan->tanggal)->isoFormat('D MMMM Y') }}
                </td>
            </tr>
        </table>

        <br>

        <table class="table table-bordered text-center" style="width: 90%;margin: 0 auto !important">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Nama Barang</th>
                    <th>Banyaknya</th>
                    <th>Harga Satuan <br> (RP)</th>
                    <th>Jumlah <br> (RP)</th>
                </tr>
                <tr style="font-size: .8rem;">
                    <td style="padding: 2px">1</td>
                    <td style="padding: 2px">2</td>
                    <td style="padding: 2px">3</td>
                    <td style="padding: 2px">4</td>
                    <td style="padding: 2px">5</td>
                </tr>
            </thead>

            <tbody>
                {{ $total = 0 }}
                @foreach ($belanjaBarang->detail as $detail)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $detail->nama_barang }}</td>
                    <td>{{ $detail->qty }} {{ $detail->satuan }}</td>
                    <td>{{ number_format($detail->harga_satuan, 0, ',', '.') }}</td>
                    <td>{{ number_format($detail->jumlah, 0, ',', '.') }}</td>
                </tr>
                {{ $total += $detail->jumlah; }}
                @endforeach

                <tr>
                    <td colspan="4" class="fw-bold text-center">Jumlah</td>
                    <td class="fw-bold text-center">{{ number_format($total,0,',','.') }}</td>
                </tr>

            </tbody>
        </table>

        <table class="w-full table " style="margin: 20px 0; font-size: .8rem">
            <tr>
                <td></td>
                <td style="width: 40% !important"></td>
                <td class="text-center">
                    Martapura, {{ Carbon\Carbon::parse($baPemeriksaan->suratPengadaan->tanggal)->isoFormat('D MMMM Y')
                    }}
                </td>
            </tr>

            <tr>
                <td></td>
                <td style="width: 40% !important"></td>
                <td class="text-center">Pejabat Pembuat Teknis Kebijakan</td>
            </tr>


            <tr>
                <td> <br><br><br><br> </td>
                <td style="width: 40% !important"> <br><br><br><br> </td>
                <td> <br><br><br><br> </td>
            </tr>

            <tr>
                <td></td>

                <td style="width: 40% !important"></td>
                <td class="text-center fw-bold">
                    <u>M. Rusydi Ansharie </u>
                </td>
            </tr>

            <tr>
                <td></td>
                <td style="width: 40% !important"></td>
                <td class="text-center fw-bold">
                    NIP. 19860804 200803 1 001
                </td>
            </tr>
        </table>
    </div>


</body>
