<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>print</title>
    <style>
        body {
            margin: 5px 20px;
            padding: 0;
            box-sizing: border-box;
            font-family: 'Times New Roman', Times, serif;
            font-size: 14px;
        }

        .w-full {
            width: 100%;
        }

        p {
            margin: 0
        }

        .page-break {
            page-break-after: always;
        }

        .va-top {
            vertical-align: top
        }

        .fw-bold {
            font-weight: 700;
        }

        .text-center {
            text-align: center
        }

        .table-special {
            border-collapse: collapse;
            border: 1px solid black;
            width: 100%;
        }

        .table-special td {
            border-right: 1px solid black;
            padding: 2px 12px;

        }

        .table {
            border-collapse: collapse
        }

        .table-bordered td,
        th {
            border: 1px solid black;
            padding: 8px 12px;
        }



        .table-pad td,
        th {
            padding: 5px 12px;
        }

        p {
            text-align: justify;
        }
    </style>
</head>

<body>
    {{-- <div class="header">
        <table class="w-full " style=" height: 100px; border-bottom: 1px double black; padding-bottom: 10px">
            <tr>
                <td style="width: 10%">
                    <img src="{{ asset('assets/banjar.png') }}" alt="logo" style="height: 100%">
                </td>
                <td style="width: auto; text-align: start;font-size: 1.5rem;padding-left: 20px ">
                    <p>
                        PEMERINTAH KABUPATEN BANJAR
                    </p>

                    <p style="font-size: 2.1rem; font-weight: bold">
                        KECAMATAN MARTAPURA
                    </p>
                    <p style="font-size: 1.1rem" class="fw-bold">
                        Jl.Sekumpul Ujung No.1 Bincau kode Pos 70651
                    </p>

                </td>
            </tr>

        </table>
    </div>

    <div class="body" style="margin-top: 10px; ">

        <table class=" w-full">
            <tr>
                <td class="va-top">

                    <table>
                        <tr>
                            <td style="padding-right: 1.5rem">Nomor</td>
                            <td>:</td>
                            <td>01/ 033 /Alat Kebersihan /KEC.MTP</td>
                        </tr>
                        <tr>
                            <td style="padding-right: 1.5rem">Lampiran</td>
                            <td>:</td>
                            <td>-</td>
                        </tr>
                        <tr>
                            <td style="padding-right: 1.5rem">Perihal</td>
                            <td>:</td>
                            <td> Pesanan Barang / Order</td>
                        </tr>
                    </table>
                </td>

                <td class="va-top">
                    <table>
                        <tr>
                            <td>
                                Martapura, 10 Februari 2000
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Kepada
                            </td>
                        </tr>
                        <tr>
                            <td>
                                MM. Al Yasmin
                            </td>
                        </tr>
                        <tr>
                            <td>
                                di-
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Martapura
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        <br> <br>

        <table>
            <tr>
                <td>
                    <p>
                        &nbsp;&nbsp;&nbsp; &nbsp;&nbsp; Sehubungan dengan rencana pengadaan <span>Alat Kebersihan</span>
                        pada Kantor Kecamatan Martapura Kabupaten
                        Banjar dengan ini mohon disediakan <span>Alat Kebersihan</span> dengan rincian spesifikasi
                        sebagai berikut :
                    </p>
                </td>
            </tr>
        </table>

        <br> <br>

        <table class="table table-bordered w-full text-center">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Nama Barang</th>
                    <th>Banyaknya</th>
                    <th>Harga Satuan (RP)</th>
                    <th>Jumlah (RP)</th>
                </tr>
                <tr style="font-size: .8rem;">
                    <td style="padding: 2px">1</td>
                    <td style="padding: 2px">2</td>
                    <td style="padding: 2px">3</td>
                    <td style="padding: 2px">4</td>
                    <td style="padding: 2px">5</td>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <td>1.</td>
                    <td style="text-align: start">Stella Car Fresh</td>
                    <td>2 Buah</td>
                    <td>20.000</td>
                    <td>40.000</td>
                </tr>
                <tr>
                    <td>2.</td>
                    <td style="text-align: start">Stella Car Fresh</td>
                    <td>2 Buah</td>
                    <td>20.000</td>
                    <td>40.000</td>
                </tr>
                <tr>
                    <td>3.</td>
                    <td style="text-align: start">Stella Car Fresh</td>
                    <td>2 Buah</td>
                    <td>20.000</td>
                    <td>40.000</td>
                </tr>
                <tr>
                    <td>4.</td>
                    <td style="text-align: start">Stella Car Fresh</td>
                    <td>2 Buah</td>
                    <td>20.000</td>
                    <td>40.000</td>
                </tr>
            </tbody>
        </table>

        <br>

        <table class="w-full">
            <tr>
                <td>
                    <p>
                        dengan ketentuan sebagai berikut :
                    </p>
                </td>
            </tr>

            <tr>
                <td>
                    <table>
                        <tr class=" va-top">
                            <td style="padding-right: 10px">1</td>
                            <td>
                                <p>
                                    Pembayaran akan dilakukan bila barang yang diorder sesuai dan dalam keadaan baik
                                    dan dinyatakan dengan Berita Acara Serah terima hasil pekerjaan.
                                </p>
                            </td>
                        </tr>
                        <tr class="va-top">
                            <td style=" padding-right: 10px">2</td>
                            <td>
                                <p>
                                    Pembayaran akan dibatalkan bila barang tidak sesuai dengan pesanan/order.
                                </p>
                            </td>
                        </tr>
                        <tr class="va-top">
                            <td style=" padding-right: 10px">3</td>
                            <td>
                                <p>
                                    Kwitansi harga atas barang tersebut di atas hanya dapat dibayarkan jika
                                    melampirkan Surat Pesanan/Order aslinya.
                                </p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr>
                <td>
                    <p>
                        Demikian disampaikan, atas kerjasamanya diucapkan terima kasih.
                    </p>
                </td>
            </tr>
        </table>

        <br>

        <table class="w-full table " style="margin: 20px 0; font-size: .8rem">

            <tr>
                <td></td>
                <td style="width: 40% !important"></td>
                <td class="text-center">Pejabat Pembuat Teknis Kebijakan</td>
            </tr>


            <tr>
                <td> <br><br><br><br> </td>
                <td style="width: 40% !important"> <br><br><br><br> </td>
                <td> <br><br><br><br> </td>
            </tr>

            <tr>
                <td></td>

                <td style="width: 40% !important"></td>
                <td class="text-center fw-bold"><u>Nama Pihak 21</u></td>
            </tr>

            <tr>
                <td></td>
                <td style="width: 40% !important"></td>
                <td class="text-center fw-bold">NIP 78788787 </td>
            </tr>
        </table>

    </div>

    <div class="page-break"></div>

    <div class="header">
        <table class="w-full " style=" height: 100px; border-bottom: 1px double black; padding-bottom: 10px">
            <tr>
                <td style="width: 10%">
                    <img src="{{ asset('assets/banjar.png') }}" alt="logo" style="height: 100%">
                </td>
                <td style="width: auto; text-align: start;font-size: 1.5rem;padding-left: 20px ">
                    <p>
                        PEMERINTAH KABUPATEN BANJAR
                    </p>

                    <p style="font-size: 2.1rem; font-weight: bold">
                        KECAMATAN MARTAPURA
                    </p>
                    <p style="font-size: 1.1rem" class="fw-bold">
                        Jl.Sekumpul Ujung No.1 Bincau kode Pos 70651
                    </p>

                </td>
            </tr>

        </table>
    </div>

    <div class="body" style="margin-top: 10px; ">

        <table class=" w-full">
            <tr>
                <td class="text-center fw-bold">
                    <u style="font-size: 1.000001rem"> BERITA ACARA PEMERIKSA BARANG/JASA </u> <br>
                    Nomor : 027 /033/ Alat Kebersihan /KEC.MTP
                </td>
            </tr>
        </table>

        <br>


        <table class="w-full">
            <tr>
                <td>
                    <p>
                        &nbsp;&nbsp;&nbsp; &nbsp;&nbsp; Pada hari ini Senin tanggal Sebelas bulan November tahun Dua
                        Ribu Sembilan Belas, bertempat di Kantor Kecamatan Martapura Kabupaten Banjar, yang bertanda
                        tangan di bawah ini :
                    </p>
                </td>
            </tr>
        </table>

        <table class="w-full" style="margin: 20px 0">
            <tr>
                <td>1. Nama</td>
                <td>:</td>
                <td>Mas'yani</td>
                <td>:</td>
                <td>Ketua / merangkap anggota</td>
            </tr>
        </table>

        <table class="w-full">
            <tr>
                <td>
                    <p>
                        &nbsp;&nbsp;&nbsp; &nbsp;&nbsp; Berdasarkan Keputusan Camat Martapura Kabupaten Banjar Nomor 13
                        tanggal 28 Pebruari 2019 tentang Pembentukan Panitia Pemeriksaan Barang/Jasa Kantor Kecamatan
                        Martapura Kabupaten Banjar dan masing-masing karena jabatannya, dengan ini menyatakan dengan
                        sebenarnya telah mengadakan dan melaksanakan pemeriksaan terhadap Barang / Jasa berupa Alat
                        Kebersihan yang dipesan dari :
                    </p>
                </td>
            </tr>
        </table>

        <table class="w-full" style="margin: 20px 0">
            <tr>
                <td>Nama Toko</td>
                <td>:</td>
                <td>Toko Glassware Sekumpul dan MM. Al Yasmin</td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td>:</td>
                <td>Martapura</td>
            </tr>
        </table>

        <table class="w-full">
            <tr>
                <td>
                    <p>
                        Sebagai realisasi Surat Pesanan / Order Nomor: 01/033/ Alat Kebersihan /KEC.MTP tanggal 08
                        November 2019 dengan jumlah / jenis barang : <b>Terlampir</b>.
                    </p>
                </td>
            </tr>
        </table>

        <table class="w-full" style="margin: 20px 0">
            <tr>
                <td>Hasil pemeriksaan dinyatakan:</td>
            </tr>
            <tr>
                <td>a. Baik</td>
            </tr>
            <tr>
                <td>b. Kurang / Tidak Baik</td>
            </tr>
        </table>

        <table class="w-full">
            <tr>
                <td>
                    <p>
                        Yang selanjutnya akan diserahkan oleh Penyedia Barang/Jasa kepada Pengurus/Bendahara Barang.
                    </p>
                </td>
            </tr>
        </table>

        <br>

        <table class="w-full">
            <tr>
                <td>
                    <p>
                        &nbsp;&nbsp;&nbsp; &nbsp;&nbsp; Demikian Berita Acara ini dibuat dalam rangkap 3 (tiga) untuk
                        dipergunakan sebagaimana mestinya.
                    </p>
                </td>
            </tr>
        </table>

        <br>

        <table class="w-full">
            <tr>
                <td class="text-center">
                    Martapura, 08 November 2019
                </td>
            </tr>
        </table>
        <table class="w-full">
            <tr>
                <td class="text-center fw-bold">
                    PANITIA PEMERIKSAAN BARANG / JASA
                </td>
            </tr>
        </table>

        <table class="w-full" style="margin-top: 20px ">
            <tr>
                <td>1. Mas'yani</td>
                <td>:</td>
                <td>1 ...............................................................</td>
            </tr>
        </table>

    </div>

    <div class="page-break"></div>

    <div class="header">
        <table class="w-full " style=" height: 100px; border-bottom: 1px double black; padding-bottom: 10px">
            <tr>
                <td style="width: 10%">
                    <img src="{{ asset('assets/banjar.png') }}" alt="logo" style="height: 100%">
                </td>
                <td style="width: auto; text-align: start;font-size: 1.5rem;padding-left: 20px ">
                    <p>
                        PEMERINTAH KABUPATEN BANJAR
                    </p>

                    <p style="font-size: 2.1rem; font-weight: bold">
                        KECAMATAN MARTAPURA
                    </p>
                    <p style="font-size: 1.1rem" class="fw-bold">
                        Jl.Sekumpul Ujung No.1 Bincau kode Pos 70651
                    </p>

                </td>
            </tr>

        </table>
    </div>

    <div class="body" style="margin-top: 10px">

        <table class="w-full">
            <tr>
                <td style="width: 50%"></td>
                <td style="width: 50%">
                    Daftar Lampiran Berita Acara
                </td>
            </tr>
            <tr>
                <td style="width: 50%"></td>
                <td style="width: 50%">
                    Serah terima hasil pekerjaan
                </td>
            </tr>
            <tr>
                <td style="width: 50%"></td>
                <td style="width: 50%">
                    Nomor : 027/ 033-01/KEC.MTP
                </td>
            </tr>
            <tr>
                <td style="width: 50%"></td>
                <td style="width: 50%">
                    Tanggal : 11 November 2019
                </td>
            </tr>
        </table>

        <br>

        <table class="table table-bordered text-center" style="width: 90%;margin: 0 auto !important">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Nama Barang</th>
                    <th>Banyaknya</th>
                    <th>Harga Satuan <br> (RP)</th>
                    <th>Jumlah <br> (RP)</th>
                </tr>
                <tr style="font-size: .8rem;">
                    <td style="padding: 2px">1</td>
                    <td style="padding: 2px">2</td>
                    <td style="padding: 2px">3</td>
                    <td style="padding: 2px">4</td>
                    <td style="padding: 2px">5</td>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <td>1.</td>
                    <td style="text-align: start">Stella Car Fresh</td>
                    <td>2 Buah</td>
                    <td>20.000</td>
                    <td>40.000</td>
                </tr>
                <tr>
                    <td>2.</td>
                    <td style="text-align: start">Stella Car Fresh</td>
                    <td>2 Buah</td>
                    <td>20.000</td>
                    <td>40.000</td>
                </tr>
                <tr>
                    <td>3.</td>
                    <td style="text-align: start">Stella Car Fresh</td>
                    <td>2 Buah</td>
                    <td>20.000</td>
                    <td>40.000</td>
                </tr>
                <tr>
                    <td>4.</td>
                    <td style="text-align: start">Stella Car Fresh</td>
                    <td>2 Buah</td>
                    <td>20.000</td>
                    <td>40.000</td>
                </tr>
            </tbody>
        </table>

        <table class="w-full table " style="margin: 20px 0; font-size: .8rem">
            <tr>
                <td></td>
                <td style="width: 40% !important"></td>
                <td class="text-center">Martapura, 10 Februari 2000</td>
            </tr>

            <tr>
                <td></td>
                <td style="width: 40% !important"></td>
                <td class="text-center">Pejabat Pembuat Teknis Kebijakan</td>
            </tr>


            <tr>
                <td> <br><br><br><br> </td>
                <td style="width: 40% !important"> <br><br><br><br> </td>
                <td> <br><br><br><br> </td>
            </tr>

            <tr>
                <td></td>

                <td style="width: 40% !important"></td>
                <td class="text-center fw-bold"><u>Nama Pihak 21</u></td>
            </tr>

            <tr>
                <td></td>
                <td style="width: 40% !important"></td>
                <td class="text-center fw-bold">NIP 78788787 </td>
            </tr>
        </table>
    </div>

    <div class="page-break"></div> --}}

    <div class="header">
        <table class="w-full " style=" height: 100px; border-bottom: 1px double black; padding-bottom: 10px">
            <tr>
                <td style="width: 10%">
                    <img src="{{ asset('assets/banjar.png') }}" alt="logo" style="height: 100%">
                </td>
                <td style="width: auto; text-align: start;font-size: 1.5rem;padding-left: 20px ">
                    <p>
                        PEMERINTAH KABUPATEN BANJAR
                    </p>

                    <p style="font-size: 2.1rem; font-weight: bold">
                        KECAMATAN MARTAPURA
                    </p>
                    <p style="font-size: 1.1rem" class="fw-bold">
                        Jl.Sekumpul Ujung No.1 Bincau kode Pos 70651
                    </p>

                </td>
            </tr>

        </table>
    </div>

    <div class="body" style="margin-top: 10px; ">

        <table class=" w-full">
            <tr>
                <td class="text-center fw-bold">
                    <span style="font-size: 1.1rem">BERITA ACARA SERAH TERIMA BARANG</span> <br>
                    Nomor : 027/033/Alat Kebersihan/KEC.MTP/2077
                </td>
            </tr>
        </table>

        <table class="w-full">
            <tr>
                <td>
                    <p>
                        &nbsp;&nbsp;&nbsp; &nbsp;&nbsp; Pada hari Senin tanggal Sebelas bulan November tahun Dua Ribu
                        Sembilan Belas, kami yang bertanda tangan di bawah ini
                    </p>
                </td>
            </tr>
        </table>


        <table class="w-full">
            <tr>
                <td class="va-top" style="padding-top: 4px">1</td>
                <td>
                    <table>
                        <tr>
                            <td class="va-top" style="width: 80px">Nama</td>
                            <td class="va-top" style="width: 10px; text-align: center">:</td>
                            <td class="va-top" style="width: auto">Hapiz</td>
                        </tr>
                        <tr>
                            <td class="va-top" style="width: 80px">NIP</td>
                            <td class="va-top" style="width: 10px; text-align: center">:</td>
                            <td class="va-top" style="width: auto">9090909</td>
                        </tr>
                        <tr>
                            <td class="va-top" style="width: 80px">Jabatan</td>
                            <td class="va-top" style="width: 10px; text-align: center">:</td>
                            <td class="va-top" style="width: auto">BOS</td>
                        </tr>
                        <tr>
                            <td class="va-top" style="width: 80px">Alamat</td>
                            <td class="va-top" style="width: 10px; text-align: center">:</td>
                            <td class="va-top" style="width: auto">Lorem ipsum dolor sit amet consectetur adipisicing
                                elit. Nisi voluptate
                                eos natus.</td>
                        </tr>
                        <tr>
                            <td class="va-top" colspan="3">Dalam hal ini disebut <b>PIHAK PERTAMA</b></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="va-top" style="padding-top: 4px">2</td>
                <td>
                    <table>
                        <tr>
                            <td class="va-top" style="width: 80px">Nama</td>
                            <td class="va-top" style="width: 10px; text-align: center">:</td>
                            <td class="va-top" style="width: auto">Hapiz jua</td>
                        </tr>
                        <tr>
                            <td class="va-top" style="width: 80px">NIP</td>
                            <td class="va-top" style="width: 10px; text-align: center">:</td>
                            <td class="va-top" style="width: auto">9090909</td>
                        </tr>
                        <tr>
                            <td class="va-top" style="width: 80px">Jabatan</td>
                            <td class="va-top" style="width: 10px; text-align: center">:</td>
                            <td class="va-top" style="width: auto">BOS</td>
                        </tr>
                        <tr>
                            <td class="va-top" style="width: 80px">Alamat</td>
                            <td class="va-top" style="width: 10px; text-align: center">:</td>
                            <td class="va-top" style="width: auto">Lorem ipsum dolor sit amet consectetur adipisicing
                                elit. Nisi voluptate
                                eos natus.</td>
                        </tr>
                        <tr>
                            <td class="va-top" colspan="3">Dalam hal ini disebut <b>PIHAK KEDUA</b></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        <br>

        <table class="w-full">
            <tr>
                <td>
                    <p>
                        Dengan ini kedua belah pihak mengadakan serah terima Alat Kebersihan sebagai berikut :
                    </p>
                </td>
            </tr>
        </table>


        <table class=" table-special" style="width: 80%">
            <tr>
                <td style="width: 20px">1.</td>
                <td>Sapu tanaman</td>
                <td>1 Buah</td>
            </tr>
            <tr>
                <td style="width: 20px">2.</td>
                <td>Sapu tanaman</td>
                <td>1 Buah</td>
            </tr>
            <tr>
                <td style="width: 20px">3.</td>
                <td>Sapu tanaman</td>
                <td>1 Buah</td>
            </tr>
            <tr>
                <td style="width: 20px">4.</td>
                <td>Sapu tanaman</td>
                <td>1 Buah</td>
            </tr>
            <tr>
                <td style="width: 20px">5.</td>
                <td>Sapu tanaman</td>
                <td>1 Buah</td>
            </tr>
        </table>


        <table class="w-full">
            <tr>
                <td>
                    <p>
                        &nbsp;&nbsp;&nbsp; &nbsp;&nbsp; Demikian Berita Acara ini dibuat untuk diketahui dan
                        dipergunakan sebagaimana mestinya.
                    </p>
                </td>
            </tr>
        </table>

        <br>

        <table class="w-full table " style="margin: 20px 0; font-size: .8rem">

            <tr>
                <td>Yang Menyerahkan</td>
                <td style="width: 40% !important"></td>
                <td>Yang Menerima</td>
            </tr>

            <tr>
                <td>PIHAK PERTAMA</td>
                <td style="width: 40% !important"></td>
                <td>PIHAK KEDUA</td>
            </tr>

            <tr>
                <td> <br><br> </td>
                <td style="width: 40% !important"> <br><br> </td>
                <td> <br><br> </td>
            </tr>

            <tr>
                <td class="fw-bold"><u>Nama Pihak Pertama</u></td>
                <td style="width: 40% !important"></td>
                <td class="fw-bold"><u>Nama Pihak Kedua</u></td>
            </tr>

            <tr>
                <td class="fw-bold">NIP 12312312321</td>
                <td style="width: 40% !important"></td>
                <td class="fw-bold">NIP 12312312321</td>
            </tr>
        </table>

        <table class="w-full table text-center" style="margin: 20px 0; font-size: .8rem">
            <tr>
                <td style="width: 30% !important"></td>
                <td>Mengetahui</td>
                <td style="width: 30% !important"></td>
            </tr>
            <tr>
                <td style="width: 30% !important"></td>
                <td>Camat Martapura</td>
                <td style="width: 30% !important"></td>
            </tr>
            <tr>
                <td style="width: 30% !important"></td>
                <td> <br><br> </td>
                <td style="width: 30% !important"></td>
            </tr>
            <tr>
                <td style="width: 30% !important"></td>
                <td class="fw-bold"><u>Bukan Hapiz</u></td>
                <td style="width: 30% !important"></td>
            </tr>
            <tr>
                <td style="width: 30% !important"></td>
                <td class="fw-bold">NIP 12342342342343243</td>
                <td style="width: 30% !important"></td>
            </tr>
        </table>

    </div>

</body>
