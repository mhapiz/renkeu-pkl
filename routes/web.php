<?php

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\BelanjaBarangController;
use App\Http\Controllers\Admin\BeritaAcaraPemeriksaanController;
use App\Http\Controllers\Admin\BeritaAcaraSerahTerimaController;
use App\Http\Controllers\Admin\PegawaiController;
use App\Http\Controllers\Admin\SuplierController;
use App\Http\Controllers\Admin\SuratPengadaanController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\Kasubag\KasubagController;
use App\Http\Controllers\Kasubag\LaporanBeritaAcaraPemeriksaanController;
use App\Http\Controllers\Kasubag\LaporanBeritaAcaraSerahTerimaController;
use App\Http\Controllers\Kasubag\LaporanSuratPengadaanController;
use App\Http\Controllers\RedirectController;
use Illuminate\Support\Facades\Route;

Route::get('redirect', [RedirectController::class, 'redirect'])->name('redirect');

Route::get('/', function () {
    return redirect()->route('redirect');
});

Route::get('/print', [AdminController::class, 'print']);

Route::get('/login', [AuthController::class, 'login'])->name('login');
Route::post('/login-process', [AuthController::class, 'loginProcess'])->name('login.process');
Route::get('/logout', [AuthController::class, 'logout'])->name('logout');

Route::prefix('admin')
    ->namespace('Admin')
    ->middleware('auth')
    ->group(function () {
        Route::get('/', [AdminController::class, 'index'])->name('admin.dashboard');

        Route::prefix('pegawai')
            ->group(function () {
                Route::get('/', [PegawaiController::class, 'index'])->name('admin.pegawai.index');
                Route::get('/get-pegawai', [PegawaiController::class, 'getPegawai'])->name('admin.pegawai.getPegawai');
                Route::get('/tambah', [PegawaiController::class, 'create'])->name('admin.pegawai.create');
                Route::post('/store', [PegawaiController::class, 'store'])->name('admin.pegawai.store');
                Route::get('/edit/{id}', [PegawaiController::class, 'edit'])->name('admin.pegawai.edit');
                Route::put('/update/{id}', [PegawaiController::class, 'update'])->name('admin.pegawai.update');
                Route::delete('/delete/{id}', [PegawaiController::class, 'delete'])->name('admin.pegawai.delete');
            });

        //make route group for suplier
        Route::prefix('suplier')
            ->group(function () {
                Route::get('/', [SuplierController::class, 'index'])->name('admin.suplier.index');
                Route::get('/get-suplier', [SuplierController::class, 'getSuplier'])->name('admin.suplier.getSuplier');
                Route::get('/tambah', [SuplierController::class, 'create'])->name('admin.suplier.create');
                Route::post('/store', [SuplierController::class, 'store'])->name('admin.suplier.store');
                Route::get('/edit/{id}', [SuplierController::class, 'edit'])->name('admin.suplier.edit');
                Route::put('/update/{id}', [SuplierController::class, 'update'])->name('admin.suplier.update');
                Route::delete('/delete/{id}', [SuplierController::class, 'delete'])->name('admin.suplier.delete');
            });

        //make route group for belanja-barang
        Route::prefix('belanja-barang')
            ->group(function () {
                Route::get('/', [BelanjaBarangController::class, 'index'])->name('admin.belanja-barang.index');
                Route::get('/get-belanja-barang', [BelanjaBarangController::class, 'getBelanjaBarang'])->name('admin.belanja-barang.getBelanjaBarang');
                Route::get('print-rekap-belanja', [BelanjaBarangController::class, 'printRekapBelanja'])->name('admin.belanja-barang.printRekapBelanja');
                Route::get('/tambah', [BelanjaBarangController::class, 'create'])->name('admin.belanja-barang.create');
                Route::get('/detail/{id}', [BelanjaBarangController::class, 'detail'])->name('admin.belanja-barang.detail');
                Route::post('/store', [BelanjaBarangController::class, 'store'])->name('admin.belanja-barang.store');
                Route::get('/edit/{id}', [BelanjaBarangController::class, 'edit'])->name('admin.belanja-barang.edit');
                Route::put('/update/{id}', [BelanjaBarangController::class, 'update'])->name('admin.belanja-barang.update');
                Route::delete('/delete/{id}', [BelanjaBarangController::class, 'delete'])->name('admin.belanja-barang.delete');
            });

        Route::prefix('surat-pengadaan')
            ->group(function () {
                Route::get('/', [SuratPengadaanController::class, 'index'])->name('admin.surat-pengadaan.index');
                Route::get('print-rekap-surat-pengadaan', [SuratPengadaanController::class, 'printRekapSuratPengadaan'])->name('admin.surat-pengadaan.printRekapSuratPengadaan');
                Route::post('/store', [SuratPengadaanController::class, 'store'])->name('admin.surat-pengadaan.store');
                Route::get('/edit/{id}', [SuratPengadaanController::class, 'edit'])->name('admin.surat-pengadaan.edit');
                Route::put('/update/{id}', [SuratPengadaanController::class, 'update'])->name('admin.surat-pengadaan.update');
                Route::get('/print/{id}', [SuratPengadaanController::class, 'printSuratPengadaan'])->name('admin.surat-pengadaan.print');
                Route::delete('/delete/{id}', [SuratPengadaanController::class, 'delete'])->name('admin.surat-pengadaan.delete');
            });

        Route::prefix('berita-acara-pemeriksaan')
            ->group(function () {
                Route::get('/', [BeritaAcaraPemeriksaanController::class, 'index'])->name('admin.berita-acara-pemeriksaan.index');
                Route::get('/print-rekap-berita-acara-pemeriksaan', [BeritaAcaraPemeriksaanController::class, 'printRekapBeritaAcaraPemeriksaan'])->name('admin.berita-acara-pemeriksaan.printRekapBeritaAcaraPemeriksaan');
                Route::post('/store', [BeritaAcaraPemeriksaanController::class, 'store'])->name('admin.berita-acara-pemeriksaan.store');
                Route::get('/edit/{id}', [BeritaAcaraPemeriksaanController::class, 'edit'])->name('admin.berita-acara-pemeriksaan.edit');
                Route::put('/update/{id}', [BeritaAcaraPemeriksaanController::class, 'update'])->name('admin.berita-acara-pemeriksaan.update');
                Route::get('/print/{id}', [BeritaAcaraPemeriksaanController::class, 'printBeritaAcaraPemeriksaan'])->name('admin.berita-acara-pemeriksaan.print');
                Route::delete('/delete/{id}', [BeritaAcaraPemeriksaanController::class, 'delete'])->name('admin.berita-acara-pemeriksaan.delete');
            });

        Route::prefix('berita-acara-serah-terima')
            ->group(function () {
                Route::get('/', [BeritaAcaraSerahTerimaController::class, 'index'])->name('admin.berita-acara-serah-terima.index');
                Route::get('/print-rekap-berita-acara-serah-terima', [BeritaAcaraSerahTerimaController::class, 'printRekapBeritaAcaraSerahTerima'])->name('admin.berita-acara-serah-terima.printRekapBeritaAcaraSerahTerima');
                Route::post('/store', [BeritaAcaraSerahTerimaController::class, 'store'])->name('admin.berita-acara-serah-terima.store');
                Route::get('/edit/{id}', [BeritaAcaraSerahTerimaController::class, 'edit'])->name('admin.berita-acara-serah-terima.edit');
                Route::put('/update/{id}', [BeritaAcaraSerahTerimaController::class, 'update'])->name('admin.berita-acara-serah-terima.update');
                Route::get('/print/{id}', [BeritaAcaraSerahTerimaController::class, 'printBeritaAcaraSerahTerima'])->name('admin.berita-acara-serah-terima.print');
                Route::delete('/delete/{id}', [BeritaAcaraSerahTerimaController::class, 'delete'])->name('admin.berita-acara-serah-terima.delete');
            });
    });

Route::prefix('kasubag')
    ->namespace('Kasubag')
    ->middleware(['auth', 'isKasubag'])
    ->group(function () {

        Route::get('/', [KasubagController::class, 'index'])->name('kasubag.dashboard');

        Route::prefix('surat-pengadaan')
            ->group(function () {
                Route::get('/', [LaporanSuratPengadaanController::class, 'index'])->name('kasubag.surat-pengadaan.index');
                Route::get('print-rekap-surat-pengadaan', [LaporanSuratPengadaanController::class, 'printRekapSuratPengadaan'])->name('kasubag.surat-pengadaan.printRekapSuratPengadaan');
                Route::get('/print/{id}', [LaporanSuratPengadaanController::class, 'printSuratPengadaan'])->name('kasubag.surat-pengadaan.print');
            });

        Route::prefix('berita-acara-pemeriksaan')
            ->group(function () {
                Route::get('/', [LaporanBeritaAcaraPemeriksaanController::class, 'index'])->name('kasubag.berita-acara-pemeriksaan.index');
                Route::get('/print-rekap-berita-acara-pemeriksaan', [LaporanBeritaAcaraPemeriksaanController::class, 'printRekapBeritaAcaraPemeriksaan'])->name('kasubag.berita-acara-pemeriksaan.printRekapBeritaAcaraPemeriksaan');
                Route::get('/print/{id}', [LaporanBeritaAcaraPemeriksaanController::class, 'printBeritaAcaraPemeriksaan'])->name('kasubag.berita-acara-pemeriksaan.print');
            });

        Route::prefix('berita-acara-serah-terima')
            ->group(function () {
                Route::get('/', [LaporanBeritaAcaraSerahTerimaController::class, 'index'])->name('kasubag.berita-acara-serah-terima.index');
                Route::get('/print-rekap-berita-acara-serah-terima', [LaporanBeritaAcaraSerahTerimaController::class, 'printRekapBeritaAcaraSerahTerima'])->name('kasubag.berita-acara-serah-terima.printRekapBeritaAcaraSerahTerima');
                Route::get('/print/{id}', [LaporanBeritaAcaraSerahTerimaController::class, 'printBeritaAcaraSerahTerima'])->name('kasubag.berita-acara-serah-terima.print');
            });
    });
