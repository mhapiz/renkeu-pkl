<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BeritaAcaraPemeriksaan extends Model
{
    use HasFactory;
    protected $table = 'tb_berita_acara_pemeriksaan';
    protected $primaryKey = 'id_berita_acara_pemeriksaan';
    protected $fillable = ['no_surat', 'surat_pengadaan_id'];

    public function suratPengadaan()
    {
        return $this->belongsTo(SuratPengadaan::class, 'surat_pengadaan_id', 'id_surat_pengadaan');
    }
}
