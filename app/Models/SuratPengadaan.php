<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SuratPengadaan extends Model
{
    use HasFactory;
    protected $table = 'tb_surat_pengadaan';
    protected $primaryKey = 'id_surat_pengadaan';
    protected $fillable = ['no_surat', 'tanggal', 'belanja_barang_id'];

    public function belanjaBarang()
    {
        return $this->belongsTo(BelanjaBarang::class, 'belanja_barang_id', 'id_belanja_barang');
    }
}
