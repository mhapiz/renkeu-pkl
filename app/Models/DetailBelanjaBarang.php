<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailBelanjaBarang extends Model
{
    use HasFactory;
    protected $table = 'tb_detail_belanja_barang';
    protected $primaryKey = 'id_detail_belanja_barang';
    protected $fillable = ['belanja_barang_id', 'nama_barang', 'qty', 'satuan', 'harga_satuan', 'jumlah'];
}
