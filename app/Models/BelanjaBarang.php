<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BelanjaBarang extends Model
{
    use HasFactory;
    protected $table = 'tb_belanja_barang';
    protected $primaryKey = 'id_belanja_barang';
    protected $fillable = ['tanggal', 'kategori', 'suplier_id', 'pegawai_id'];

    public function suplier()
    {
        return $this->belongsTo(Suplier::class, 'suplier_id', 'id_suplier');
    }

    public function pegawai()
    {
        return $this->belongsTo(Pegawai::class, 'pegawai_id', 'id_pegawai');
    }

    public function detail()
    {
        return $this->hasMany(DetailBelanjaBarang::class, 'belanja_barang_id', 'id_belanja_barang');
    }
}
