<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BeritaAcaraSerahTerima extends Model
{
    use HasFactory;
    protected $table = 'tb_berita_acara_serah_terima';
    protected $primaryKey = 'id_berita_acara_serah_terima';
    protected $fillable = ['no_surat', 'berita_acara_pemeriksaan_id'];

    public function baPemeriksaan()
    {
        return $this->belongsTo(BeritaAcaraPemeriksaan::class, 'berita_acara_pemeriksaan_id', 'id_berita_acara_pemeriksaan');
    }

    public function suratPengadaan()
    {
        return $this->hasOneThrough(SuratPengadaan::class, BeritaAcaraPemeriksaan::class, 'id_berita_acara_pemeriksaan', 'id_surat_pengadaan', 'berita_acara_pemeriksaan_id', 'id_surat_pengadaan');
    }
}
