<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BelanjaBarang;
use App\Models\BeritaAcaraPemeriksaan;
use App\Models\DetailBelanjaBarang;
use App\Models\SuratPengadaan;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use Riskihajar\Terbilang\Facades\Terbilang;

class BeritaAcaraPemeriksaanController extends Controller
{
    public function index()
    {
        $baPemeriksaan = BeritaAcaraPemeriksaan::with('suratPengadaan')->latest()->get();
        $suratPengadaan = SuratPengadaan::whereNotIn('id_surat_pengadaan', $baPemeriksaan->pluck('surat_pengadaan_id'))->get();
        return view('pages.backend.berita-acara-pemeriksaan.index', [
            'suratPengadaan' => $suratPengadaan,
            'baPemeriksaan' => $baPemeriksaan

        ]);
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'no_surat' => 'required',
            'surat_pengadaan_id' => 'required',
        ]);

        BeritaAcaraPemeriksaan::create($data);

        Alert::success('Berhasil', 'Berita Acara Pemeriksaan Berhasil Dibuat');
        return redirect()->route('admin.berita-acara-pemeriksaan.index');
    }

    public function edit($id)
    {
        $baPemeriksaan = BeritaAcaraPemeriksaan::findOrFail($id);
        $suratPengadaan = SuratPengadaan::whereNotIn('id_surat_pengadaan', $baPemeriksaan->pluck('surat_pengadaan_id'))->get();
        return view('pages.backend.berita-acara-pemeriksaan.edit', [
            'baPemeriksaan' => $baPemeriksaan,
            'suratPengadaan' => $suratPengadaan
        ]);
    }

    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'no_surat' => 'required',
            'surat_pengadaan_id' => 'required',
        ]);

        BeritaAcaraPemeriksaan::findOrFail($id)->update($data);

        Alert::success('Berhasil', 'Berita Acara Pemeriksaan Berhasil Diubah');
        return redirect()->route('admin.berita-acara-pemeriksaan.index');
    }

    public function printBeritaAcaraPemeriksaan($id)
    {
        $baPemeriksaan = BeritaAcaraPemeriksaan::with('suratPengadaan')->findOrFail($id);
        $belanjaBarang = BelanjaBarang::with(['detail', 'suplier'])->where('id_belanja_barang', '=', $baPemeriksaan->suratPengadaan->belanja_barang_id)->first();

        //get year with carbon
        $tahun = ucwords(Terbilang::make(Carbon::parse($baPemeriksaan->suratPengadaan->tanggal)->format('Y')));
        $bulan = ucwords(Carbon::parse($baPemeriksaan->suratPengadaan->tanggal)->isoformat('MMMM'));
        $tanggal = ucwords(Terbilang::make(Carbon::parse($baPemeriksaan->suratPengadaan->tanggal)->isoformat('D')));
        $hari = ucwords(Carbon::parse($baPemeriksaan->suratPengadaan->tanggal)->isoformat('dddd'));

        $tgl = [
            'tahun' => $tahun,
            'bulan' => $bulan,
            'tanggal' => $tanggal,
            'hari' => $hari,
        ];


        $pdf = PDF::loadView('print.ba-pemeriksaan', [
            'baPemeriksaan' => $baPemeriksaan,
            'belanjaBarang' => $belanjaBarang,
            'tgl' => $tgl,
        ]);
        return $pdf->stream();
    }

    public function printRekapBeritaAcaraPemeriksaan()
    {
        $baPemeriksaan = BeritaAcaraPemeriksaan::with('suratPengadaan')->latest()->get();
        $pdf = PDF::loadView('print.rekap-ba-pemeriksaan', [
            'baPemeriksaan' => $baPemeriksaan,
        ]);
        return $pdf->setPaper('a4', 'landscape')->stream();
    }

    public function delete($id)
    {
        $baPemeriksaan = BeritaAcaraPemeriksaan::findOrFail($id);
        $baPemeriksaan->delete();
        Alert::success('Berhasil', 'Berita Acara Pemeriksaan Berhasil Dihapus');
        return redirect()->route('admin.berita-acara-pemeriksaan.index');
    }
}
