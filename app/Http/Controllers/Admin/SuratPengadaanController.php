<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BelanjaBarang;
use App\Models\DetailBelanjaBarang;
use App\Models\SuratPengadaan;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class SuratPengadaanController extends Controller
{
    public function index()
    {
        $suratPengadaan = SuratPengadaan::with('belanjaBarang')->latest()->get();
        $belanjaBarang = BelanjaBarang::with(['suplier'])
            ->whereNotIn('id_belanja_barang', $suratPengadaan->pluck('belanja_barang_id'))
            ->get();

        return view('pages.backend.surat-pengadaan.index', [
            'belanjaBarang' => $belanjaBarang, 'suratPengadaan' => $suratPengadaan
        ]);
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'no_surat' => 'required',
            'tanggal' => 'required',
            'belanja_barang_id' => 'required',
        ]);

        SuratPengadaan::create($data);

        Alert::success('Surat Pengadaan berhasil dibuat', 'Berhasil');
        return redirect()->route('admin.surat-pengadaan.index');
    }

    public function edit($id)
    {
        $suratPengadaan = SuratPengadaan::findOrFail($id);
        $belanjaBarangE = BelanjaBarang::with(['suplier'])
            ->whereNotIn('id_belanja_barang', $suratPengadaan->pluck('belanja_barang_id'))
            ->get();
        return view('pages.backend.surat-pengadaan.edit', [
            'suratPengadaan' => $suratPengadaan, 'belanjaBarangE' => $belanjaBarangE
        ]);
    }

    public function printSuratPengadaan($id)
    {
        $suratPengadaan = SuratPengadaan::with('belanjaBarang')->find($id);
        $belanjaBarang = BelanjaBarang::where('id_belanja_barang', $suratPengadaan->belanja_barang_id)->with('suplier')->first();
        $detailBelanja = DetailBelanjaBarang::where('belanja_barang_id', $suratPengadaan->belanja_barang_id)->get();

        $pdf = PDF::loadView('print.surat-pengadaan', [
            'suratPengadaan' => $suratPengadaan,
            'belanjaBarang' => $belanjaBarang,
            'detailBelanja' => $detailBelanja,
        ]);
        return $pdf->stream();
    }

    public function printRekapSuratPengadaan()
    {
        $suratPengadaan = SuratPengadaan::with('belanjaBarang')->latest()->get();


        $pdf = PDF::loadView('print.rekap-surat-pengadaan', [
            'suratPengadaan' => $suratPengadaan,
        ]);
        return $pdf->setPaper('a4', 'landscape')->stream();
    }

    public function delete($id)
    {
        $suratPengadaan = SuratPengadaan::findOrFail($id);
        $suratPengadaan->delete();

        Alert::warning('Surat Pengadaan berhasil dihapus', 'Berhasil');
        return redirect()->route('admin.surat-pengadaan.index');
    }
}
