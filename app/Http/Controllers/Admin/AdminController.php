<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BelanjaBarang;
use App\Models\BeritaAcaraPemeriksaan;
use App\Models\BeritaAcaraSerahTerima;
use App\Models\Pegawai;
use App\Models\Suplier;
use App\Models\SuratPengadaan;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {
        //count data on pegawais
        $jumlahPegawai = Pegawai::count();
        $jumlahSuplier = Suplier::count();
        $jumlahBelanjaBarang = BelanjaBarang::count();
        $jumlahSuratPengadaan = SuratPengadaan::count();
        $jumlahBaPemeriksaan  = BeritaAcaraPemeriksaan::count();
        $jumlahBaSerahTerima = BeritaAcaraSerahTerima::count();

        return view('pages.backend.dashboard', [
            'jumlahPegawai' => $jumlahPegawai,
            'jumlahSuplier' => $jumlahSuplier,
            'jumlahBelanjaBarang' => $jumlahBelanjaBarang,
            'jumlahSuratPengadaan' => $jumlahSuratPengadaan,
            'jumlahBaPemeriksaan' => $jumlahBaPemeriksaan,
            'jumlahBaSerahTerima' => $jumlahBaSerahTerima
        ]);
    }

    function print()
    {
        $pdf = PDF::loadView('print.rekap', []);
        return $pdf->setPaper('a4', 'landscape')->stream();
    }
}
