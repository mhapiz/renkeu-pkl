<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Suplier;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use Yajra\DataTables\Facades\DataTables;

class SuplierController extends Controller
{
    public function index()
    {
        return view('pages.backend.suplier.index');
    }

    public function getSuplier()
    {
        $data = Suplier::latest()->get();
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('aksi', function ($row) {
                $editUrl = route('admin.suplier.edit', $row->id_suplier);
                $deleteUrl = route('admin.suplier.delete', $row->id_suplier);

                return view('modules.backend._formActions', compact('editUrl', 'deleteUrl'));
            })
            ->rawColumns(['aksi'])
            ->make(true);
    }

    public function create()
    {
        return view('pages.backend.suplier.create');
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'nama_suplier' => 'required|string|max:255',
            'kota' => 'required|string|max:255',
            'alamat' => 'required',
            'nohp' => 'required|string|max:20',
        ]);

        Suplier::create($data);

        if ($request->quickStore != null) {
            Alert::success('Data Suplier berhasil ditambahkan', 'Selamat');
            return redirect()->back();
        } else {

            Alert::success('Data berhasil ditambahkan', 'Selamat');
            return redirect()->route('admin.suplier.index');
        }
    }

    public function edit($id)
    {
        $data = Suplier::findOrFail($id);
        return view('pages.backend.suplier.edit', [
            'data' => $data
        ]);
    }

    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'nama_suplier' => 'required|string|max:255',
            'kota' => 'required|string|max:255',
            'alamat' => 'required',
            'nohp' => 'required|string|max:20',
        ]);

        Suplier::findOrFail($id)->update($data);

        Alert::success('Berhasil', 'Data berhasil diubah');
        return redirect()->route('admin.suplier.index');
    }

    public function delete($id)
    {
        $data = Suplier::findOrFail($id);
        $data->delete();

        Alert::success('Berhasil', 'Data berhasil dihapus');
        return redirect()->route('admin.suplier.index');
    }
}
