<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Pegawai;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use Yajra\DataTables\Facades\DataTables;

class PegawaiController extends Controller
{
    public function index()
    {
        return view('pages.backend.pegawai.index');
    }

    public function getPegawai()
    {
        $data = Pegawai::latest()->get();
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('aksi', function ($row) {
                $editUrl = route('admin.pegawai.edit', $row->id_pegawai);
                $deleteUrl = route('admin.pegawai.delete', $row->id_pegawai);

                return view('modules.backend._formActions', compact('editUrl', 'deleteUrl'));
            })
            ->rawColumns(['aksi'])
            ->make(true);
    }

    public function create()
    {
        return view('pages.backend.pegawai.create');
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'nama_pegawai' => 'required|string|max:255',
            'nip' => 'required|string|max:255',
            'jabatan' => 'required|string|max:255',
            'nohp' => 'required|string|max:255',
            'alamat' => 'required|string|max:255',
        ]);

        Pegawai::create($data);

        Alert::success('Berhasil', 'Data berhasil ditambahkan');
        return redirect()->route('admin.pegawai.index');
    }

    public function edit($id)
    {
        $data = Pegawai::findOrFail($id);
        return view('pages.backend.pegawai.edit', [
            'data' => $data
        ]);
    }

    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'nama_pegawai' => 'required|string|max:255',
            'nip' => 'required|string|max:255',
            'jabatan' => 'required|string|max:255',
            'nohp' => 'required|string|max:255',
            'alamat' => 'required|string|max:255',
        ]);

        $pegawai = Pegawai::findOrFail($id);

        $pegawai->update($data);

        Alert::success('Berhasil', 'Data berhasil diubah');
        return redirect()->route('admin.pegawai.index');
    }

    public function delete($id)
    {
        $pegawai = Pegawai::findOrFail($id);
        $pegawai->delete();

        Alert::success('Berhasil', 'Data berhasil dihapus');
        return redirect()->route('admin.pegawai.index');
    }
}
