<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BelanjaBarang;
use App\Models\DetailBelanjaBarang;
use App\Models\Pegawai;
use App\Models\Suplier;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use Yajra\DataTables\Facades\DataTables;

class BelanjaBarangController extends Controller
{
    public function index()
    {

        return view('pages.backend.belanja-barang.index');
    }

    public function getBelanjaBarang()
    {
        $data = BelanjaBarang::with(['suplier', 'pegawai', 'detail'])->latest()->get();
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('aksi', function ($row) {
                $editUrl = route('admin.belanja-barang.edit', $row->id_belanja_barang);
                $detailUrl = route('admin.belanja-barang.detail', $row->id_belanja_barang);
                $deleteUrl = route('admin.belanja-barang.delete', $row->id_belanja_barang);

                return view('modules.backend._specialActions', compact('editUrl', 'detailUrl', 'deleteUrl'));
            })
            ->editColumn('tanggal', function ($row) {
                return Carbon::parse($row->tanggal)->isoFormat('dddd, D MMMM Y');
            })
            ->editColumn('total_belanja', function ($row) {
                $total = 0;
                foreach ($row->detail as $detail) {
                    $total += $detail->jumlah;
                }
                return 'Rp. ' . number_format($total, 0, ',', '.');
            })
            ->rawColumns(['aksi', 'tanggal', 'total_belanja'])
            ->make(true);
    }

    public function printRekapBelanja()
    {
        $belanja = BelanjaBarang::with(['suplier', 'pegawai', 'detail'])->latest()->get();

        $pdf = PDF::loadView('print.rekap-belanja', [
            'belanja' => $belanja
        ]);
        return $pdf->setPaper('a4', 'landscape')->stream();
    }

    // make detail function
    public function detail($id)
    {
        $belanjaBarang = BelanjaBarang::find($id);
        $detailBelanjaBarang = DetailBelanjaBarang::where('belanja_barang_id', $id)->get();
        return view('pages.backend.belanja-barang.detail', [
            'belanjaBarang' => $belanjaBarang,
            'detailBelanjaBarang' => $detailBelanjaBarang
        ]);
    }

    public function create()
    {
        $suplier = Suplier::all();
        $pegawai = Pegawai::all();

        return view('pages.backend.belanja-barang.create', [
            'suplier' => $suplier, 'pegawai' => $pegawai

        ]);
    }

    public function store(Request $request)
    {

        $request->validate([
            'suplier_id' => 'required',
            'pegawai_id' => 'required',
            'tanggal' => 'required',
            'kategori' => 'required',
            'nama_barang' => 'required',
            'qty' => 'required',
            'satuan' => 'required',
            'harga_satuan' => 'required',
        ]);

        $belanjaBarang = BelanjaBarang::create([
            'tanggal' => $request->tanggal,
            'kategori' => $request->kategori,
            'suplier_id' => $request->suplier_id,
            'pegawai_id' => $request->pegawai_id,
        ]);

        foreach ($request->nama_barang as $key => $value) {
            DetailBelanjaBarang::create([
                'belanja_barang_id' => $belanjaBarang->id_belanja_barang,
                'nama_barang' => $request->nama_barang[$key],
                'qty' => $request->qty[$key],
                'satuan' => $request->satuan[$key],
                'harga_satuan' => $request->harga_satuan[$key],
                'jumlah' => $request->qty[$key] * $request->harga_satuan[$key],
            ]);
        }

        Alert::success('Berhasil', 'Data berhasil ditambahkan');
        return redirect()->route('admin.belanja-barang.index');
    }

    public function edit($id)
    {
        $belanjaBarang = BelanjaBarang::findOrFail($id);
        $detailBelanjaBarang = DetailBelanjaBarang::where('belanja_barang_id', $id)->get();
        $suplier = Suplier::all();
        $pegawai = Pegawai::all();

        return view('pages.backend.belanja-barang.edit', [
            'belanjaBarang' => $belanjaBarang,
            'detailBelanjaBarang' => $detailBelanjaBarang,
            'suplier' => $suplier,
            'pegawai' => $pegawai
        ]);
    }

    // make update function
    public function update(Request $request, $id)
    {
        $request->validate([
            'suplier_id' => 'required',
            'pegawai_id' => 'required',
            'tanggal' => 'required',
            'kategori' => 'required',
            'nama_barang' => 'required',
            'qty' => 'required',
            'satuan' => 'required',
            'harga_satuan' => 'required',
        ]);

        $belanjaBarang = BelanjaBarang::findOrFail($id);
        $belanjaBarang->update([
            'tanggal' => $request->tanggal,
            'kategori' => $request->kategori,
            'suplier_id' => $request->suplier_id,
            'pegawai_id' => $request->pegawai_id,
        ]);

        DetailBelanjaBarang::where('belanja_barang_id', $id)->delete();

        foreach ($request->nama_barang as $key => $value) {
            DetailBelanjaBarang::create([
                'belanja_barang_id' => $belanjaBarang->id_belanja_barang,
                'nama_barang' => $request->nama_barang[$key],
                'qty' => $request->qty[$key],
                'satuan' => $request->satuan[$key],
                'harga_satuan' => $request->harga_satuan[$key],
                'jumlah' => $request->qty[$key] * $request->harga_satuan[$key],
            ]);
        }

        Alert::success('Berhasil', 'Data berhasil diubah');
        return redirect()->route('admin.belanja-barang.index');
    }

    // make delete function
    public function delete($id)
    {
        $belanjaBarang = BelanjaBarang::findOrFail($id);
        DetailBelanjaBarang::where('belanja_barang_id', $id)->delete();
        $belanjaBarang->delete();

        Alert::success('Berhasil', 'Data berhasil dihapus');
        return redirect()->route('admin.belanja-barang.index');
    }
}
