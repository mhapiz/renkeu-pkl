<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DummyController extends Controller
{
    public function admin()
    {
        return view('pages.backend.dashboard');
    }
}
