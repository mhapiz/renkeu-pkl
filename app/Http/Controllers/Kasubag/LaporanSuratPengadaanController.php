<?php

namespace App\Http\Controllers\Kasubag;

use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use App\Models\BelanjaBarang;
use App\Models\SuratPengadaan;
use App\Models\DetailBelanjaBarang;
use App\Http\Controllers\Controller;

class LaporanSuratPengadaanController extends Controller
{
    public function index()
    {
        $suratPengadaan = SuratPengadaan::with('belanjaBarang')->latest()->get();
        $belanjaBarang = BelanjaBarang::with(['suplier'])
            ->whereNotIn('id_belanja_barang', $suratPengadaan->pluck('belanja_barang_id'))
            ->get();

        return view('pages.backend.kasubag.surat-pengadaan.index', [
            'belanjaBarang' => $belanjaBarang, 'suratPengadaan' => $suratPengadaan
        ]);
    }

    public function printSuratPengadaan($id)
    {
        $suratPengadaan = SuratPengadaan::with('belanjaBarang')->find($id);
        $belanjaBarang = BelanjaBarang::where('id_belanja_barang', $suratPengadaan->belanja_barang_id)->with('suplier')->first();
        $detailBelanja = DetailBelanjaBarang::where('belanja_barang_id', $suratPengadaan->belanja_barang_id)->get();

        $pdf = PDF::loadView('print.surat-pengadaan', [
            'suratPengadaan' => $suratPengadaan,
            'belanjaBarang' => $belanjaBarang,
            'detailBelanja' => $detailBelanja,
        ]);
        return $pdf->stream();
    }

    public function printRekapSuratPengadaan()
    {
        $suratPengadaan = SuratPengadaan::with('belanjaBarang')->latest()->get();

        $pdf = PDF::loadView('print.rekap-surat-pengadaan', [
            'suratPengadaan' => $suratPengadaan,
        ]);
        return $pdf->setPaper('a4', 'landscape')->stream();
    }
}
