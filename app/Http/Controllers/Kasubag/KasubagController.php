<?php

namespace App\Http\Controllers\Kasubag;

use Illuminate\Http\Request;
use App\Models\SuratPengadaan;
use App\Http\Controllers\Controller;
use App\Models\BeritaAcaraPemeriksaan;
use App\Models\BeritaAcaraSerahTerima;

class KasubagController extends Controller
{
    public function index()
    {
        $jumlahSuratPengadaan = SuratPengadaan::count();
        $jumlahBaPemeriksaan  = BeritaAcaraPemeriksaan::count();
        $jumlahBaSerahTerima = BeritaAcaraSerahTerima::count();

        return view('pages.backend.kasubag.dashboard', [
            'jumlahSuratPengadaan' => $jumlahSuratPengadaan,
            'jumlahBaPemeriksaan' => $jumlahBaPemeriksaan,
            'jumlahBaSerahTerima' => $jumlahBaSerahTerima,
        ]);
    }
}
