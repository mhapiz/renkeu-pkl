<?php

namespace App\Http\Controllers\Kasubag;

use Illuminate\Http\Request;
use App\Models\BelanjaBarang;
use App\Models\SuratPengadaan;
use App\Http\Controllers\Controller;
use App\Models\BeritaAcaraPemeriksaan;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Riskihajar\Terbilang\Facades\Terbilang;

class LaporanBeritaAcaraPemeriksaanController extends Controller
{
    public function index()
    {
        $baPemeriksaan = BeritaAcaraPemeriksaan::with('suratPengadaan')->latest()->get();
        $suratPengadaan = SuratPengadaan::whereNotIn('id_surat_pengadaan', $baPemeriksaan->pluck('surat_pengadaan_id'))->get();
        return view('pages.backend.kasubag.berita-acara-pemeriksaan.index', [
            'suratPengadaan' => $suratPengadaan,
            'baPemeriksaan' => $baPemeriksaan

        ]);
    }
    public function printBeritaAcaraPemeriksaan($id)
    {
        $baPemeriksaan = BeritaAcaraPemeriksaan::with('suratPengadaan')->findOrFail($id);
        $belanjaBarang = BelanjaBarang::with(['detail', 'suplier'])->where('id_belanja_barang', '=', $baPemeriksaan->suratPengadaan->belanja_barang_id)->first();

        //get year with carbon
        $tahun = ucwords(Terbilang::make(Carbon::parse($baPemeriksaan->suratPengadaan->tanggal)->format('Y')));
        $bulan = ucwords(Carbon::parse($baPemeriksaan->suratPengadaan->tanggal)->isoformat('MMMM'));
        $tanggal = ucwords(Terbilang::make(Carbon::parse($baPemeriksaan->suratPengadaan->tanggal)->isoformat('D')));
        $hari = ucwords(Carbon::parse($baPemeriksaan->suratPengadaan->tanggal)->isoformat('dddd'));

        $tgl = [
            'tahun' => $tahun,
            'bulan' => $bulan,
            'tanggal' => $tanggal,
            'hari' => $hari,
        ];


        $pdf = PDF::loadView('print.ba-pemeriksaan', [
            'baPemeriksaan' => $baPemeriksaan,
            'belanjaBarang' => $belanjaBarang,
            'tgl' => $tgl,
        ]);
        return $pdf->stream();
    }

    public function printRekapBeritaAcaraPemeriksaan()
    {
        $baPemeriksaan = BeritaAcaraPemeriksaan::with('suratPengadaan')->latest()->get();
        $pdf = PDF::loadView('print.rekap-ba-pemeriksaan', [
            'baPemeriksaan' => $baPemeriksaan,
        ]);
        return $pdf->setPaper('a4', 'landscape')->stream();
    }
}
