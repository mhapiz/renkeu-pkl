<?php

namespace App\Http\Controllers\Kasubag;

use Illuminate\Http\Request;
use App\Models\BelanjaBarang;
use App\Models\SuratPengadaan;
use App\Http\Controllers\Controller;
use App\Models\BeritaAcaraPemeriksaan;
use App\Models\BeritaAcaraSerahTerima;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Riskihajar\Terbilang\Facades\Terbilang;

class LaporanBeritaAcaraSerahTerimaController extends Controller
{
    public function index()
    {
        $baSerahTerima = BeritaAcaraSerahTerima::with('baPemeriksaan')->latest()->get();
        $baPemeriksaan = BeritaAcaraPemeriksaan::whereNotIn('id_berita_acara_pemeriksaan', $baSerahTerima->pluck('berita_acara_pemeriksaan_id'))->get();
        return view('pages.backend.kasubag.berita-acara-serah-terima.index', [
            'baSerahTerima' => $baSerahTerima,
            'baPemeriksaan' => $baPemeriksaan
        ]);
    }


    public function printBeritaAcaraSerahTerima($id)
    {

        $baSerahTerima = BeritaAcaraSerahTerima::with('baPemeriksaan')->findOrFail($id);
        $suratPengadaan = SuratPengadaan::select(['id_surat_pengadaan', 'belanja_barang_id', 'tanggal'])->where('id_surat_pengadaan', $baSerahTerima->baPemeriksaan->surat_pengadaan_id)->first();
        $belanjaBarang = BelanjaBarang::with(['detail', 'suplier'])->where('id_belanja_barang', '=', $suratPengadaan->belanja_barang_id)->first();

        //get year with carbon
        $tahun = ucwords(Terbilang::make(Carbon::parse($suratPengadaan->tanggal)->format('Y')));
        $bulan = ucwords(Carbon::parse($suratPengadaan->tanggal)->isoformat('MMMM'));
        $tanggal = ucwords(Carbon::parse($suratPengadaan->tanggal)->isoformat('D'));
        $hari = ucwords(Carbon::parse($suratPengadaan->tanggal)->isoformat('dddd'));

        $tgl = [
            'tahun' => $tahun,
            'bulan' => $bulan,
            'tanggal' => $tanggal,
            'hari' => $hari,
        ];


        $pdf = PDF::loadView('print.ba-serah-terima', [
            'baSerahTerima' => $baSerahTerima,
            'belanjaBarang' => $belanjaBarang,
            'tgl' => $tgl,
        ]);

        return $pdf->stream();
    }

    public function printRekapBeritaAcaraSerahTerima()
    {
        $baSerahTerima = BeritaAcaraSerahTerima::with('baPemeriksaan')->latest()->get();
        $pdf = PDF::loadView('print.rekap-ba-serah-terima', [
            'baSerahTerima' => $baSerahTerima,
        ]);

        return $pdf->setPaper('a4', 'landscape')->stream();
    }
}
