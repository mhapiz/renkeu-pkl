<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RedirectController extends Controller
{
    public function redirect()
    {
        //check role user
        if (Auth::check() && Auth::user()->role == 'KASUBAG') {
            return redirect()->route('kasubag.dashboard');
        } elseif (Auth::check() && Auth::user()->role == 'ADMIN') {
            return redirect()->route('admin.dashboard');
        } else {
            return redirect()->route('login');
        }
    }
}
